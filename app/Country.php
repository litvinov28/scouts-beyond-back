<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends BaseModel
{
    protected $fillable = ['name'];

    public function player(){
        return $this->hasMany('App\Player');
    }
    public function city(){
        return $this->hasMany('App\City');
    }
}
