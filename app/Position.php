<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Position extends BaseModel
{
    protected $fillable = ['position','type', 'key_id'];

    public function field_position(){
        return $this->hasMany('App\Player');
    }

    public function preffered_position(){
        return $this->hasMany('App\Player','preffered_position_id');
    }

    public function keys(){
        return $this->belongsTo('App\Key','key_id');
    }

    public function language(){
        return $this->belongsTo('App\languageSite','language_id');
    }
}
