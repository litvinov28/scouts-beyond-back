<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUS extends BaseModel
{
    public $table = 'contact_us';

    public $fillable = [
        'fullname',
        'subject_id',
        'phone',
        'email',
        'message',
        'language_id'
    ];

    public function subject()
    {
        return $this->belongsTo('App\Subject');
    }

    public function language(){
        return $this->belongsTo('App\languageSite','language_id');
    }
}
