<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Player extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'avatar_src',
    ];

    protected $casts = [
        'is_active' => 'boolean',
        'intrested_in_foreign' => 'boolean',
        'has_agent' => 'boolean',
        'has_contract' => 'boolean',
    ];
    protected $appends = ['firstname','lastname'];

    /*** RELATIONS ***/
//    protected $dateFormat = 'd-m-Y';
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getFirstnameAttribute()
    {
        return $this->user->firstname ?? '';
    }
    public function getLastnameAttribute()
    {
        return $this->user->lastname ?? '';
    }


    public function reports()
    {
        return $this->hasMany('App\Report');
    }

    public function experience()
    {
        return $this->belongsTo('App\Experience');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function field_position()
    {
        return $this->belongsTo('App\Position', 'field_position_id');
    }

    public function preffered_position()
    {
        return $this->belongsTo('App\Position');
    }


    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    public function player_type()
    {
        return $this->belongsTo('App\PlayerType');
    }


    /*** END-RELATIONS ***/

    /*** SCOPES ***/

    public function scopeGetByUser($query, $user)
    {
        return $query->where('user_id', $user->id)->first();
    }

    public function scopeByPosition($query, $position)
    {
        if (!empty($position)) {
            return $query->whereIn('field_position_id', $position);
        }
        return $query;
    }

    public function scopeByExperience($query, $experince)
    {
        if (!empty($experince)) {
            return $query->whereIn('experience_id', $experince);
        }
        return $query;
    }

    public function scopeByGender($query, $gender)
    {
        if (!empty($gender)) {
            return $query->whereIn('gender_id', $gender);
        }
        return $query;
    }

    public function scopeByCountry($query, $country)
    {
        if(!empty($country)){
            return $query->whereIn('country_id',$country);
        }
        return $query;
    }
    /*** END-SCOPES ***/

    /** ACCESSORS **/

    public function getContractDateAttribute($date)
    {
        if ($date)
            return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getDateOfBirthAttribute($date)
    {
        if ($date)
            return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function getResumeAttribute($value)
    {
        $path = config('constants.file_folder.resume.get_path');
        return url($path.$value);
    }

    /** END ACCESSORS **/

}
