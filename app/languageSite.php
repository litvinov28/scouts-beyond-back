<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class languageSite extends Model
{
    //
    protected $fillable = [
        'name',
    ];

    public function abouts(){
        return $this->hasMany('App\About');
    }

    public function howItWork(){
        return $this->hasMany('App\HowItWork');
    }

    public function contactUs(){
        return $this->hasMany('App\ContactUs');
    }

    public function banner(){
        return $this->hasMany('App\Banner');
    }

    public function staticText(){
        return $this->hasMany('App\StaticText');
    }

    public function dynamicText(){
        return $this->hasMany('App\DynamicText');
    }

    public function profileButton(){
        return $this->hasMany('App\ProfileButton');
    }

    public function key(){
        return $this->hasMany('App\Key');
    }

    public function slider(){
        return $this->hasMany('App\Slider');

    }

    public function users(){
        return $this->hasMany('App\User');
    }

    public function mail(){
        return $this->hasMany('App\Mail');
    }

    public function position(){
        return $this->hasMany('App\Position');
    }

    public function adminVideo(){
        return $this->hasMany('App\AdminVideo');
    }
}
