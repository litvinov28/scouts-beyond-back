<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DynamicText extends BaseModel
{
    protected $fillable = [
        'title',
        'field1',
        'field2',
        'field3',
        'field4',
        'field5',
        'field6',
        'field7',
        'field8',
        'field9',
        'language_id'
    ];

    public function language(){
        return $this->belongsTo('App\languageSite', 'language_id');
    }
}
