<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;

class AdminVideo extends BaseModel
{
    protected $fillable = [
        'user_id',
        'path',
        'section',
        'type',
        'description',
        'language_id',
        'key_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function getUrlAttribute() {
        $value = $this->path;
        if ($value) {
            return 'https://player.vimeo.com/video/' . $value;
        }
    }

    public function keys()
    {
        return $this->belongsTo('App\Key');
    }

    public function language()
    {
        return $this->belongsTo('App\languageSite');
    }
}
