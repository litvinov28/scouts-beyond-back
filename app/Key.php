<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    protected $fillable =

        [
            'name'
        ];
    public function language(){
        return $this->belongsTo('App\LanguageSite','language_id');
    }

    public function menu(){
        return $this->hasMany('App\Menu');
    }
    public function buttons()
    {
        return $this->hasMany('App\ProfileButton','key_id');
    }

    public function gender(){
        return $this->hasMany('App\Gender', 'key_id');
    }

    public function subject(){
        return $this->hasMany('App\Subject', 'key_id');
    }

    public function howItWork(){
        return $this->hasMany('App\HowItWork', 'key_id');
    }

    public function languages(){
        return $this->hasMany('App\Language','key_id');
    }

    public function slider(){
        return $this->hasMany('App\Slider','key_id');

    }

    public function aboutUs()
    {
        return $this->hasMany('App\About', 'key_id');
    }

    public function mail()
    {
        return $this->hasMany('App\Mail', 'key_id');
    }

    public function position()
    {
        return $this->hasMany('App\Position', 'key_id');
    }

    public function adminVideo()
    {
        return $this->hasMany('App\AdminVideo', 'key_id');
    }
}
