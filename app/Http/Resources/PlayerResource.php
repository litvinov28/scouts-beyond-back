<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Config;

class PlayerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $user = \App\User::withTrashed()->find($this->user_id);

        $avatar_src = null;
        $getPath = Config::get('constants.image_folder.avatars.get_path');
        if ($this->avatar_src) {
            $avatar_src = url($getPath . $this->avatar_src);
        } else {
            $avatar_src = $this->avatar_url;
        }
        return [
            'id' => $user->id,
            'user_id' => $user->id,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'email' => $user->email,
            'popup' => $user->popup,
            'parent_phone' => $user->parent_phone,
            'verified' => $user->verified,
            'reports' => \App\Http\Resources\ReportResource::collection($this->reports),
            'video' => $user->uservideo ?? null,
            'role' => \App\Role::find($user->role_id)->name,
            'avatar' => $avatar_src,
            'date_of_birth' => $this->date_of_birth,
            'country' => [
                'id' => $this->country->id ?? null,
                'name' => $this->country->name ?? null,
            ],
            'city' => [
                'id' => $this->city->id ?? null,
                'name' => $this->city->name ?? null,
            ],
            'soccer_team' => $this->soccer_team,
            'resume' => $this->resume,
            'player_type' => [
                'id' => $this->player_type->id ?? null,
                'name' => $this->player_type->name ?? null,
            ],
            'experience_id' => $this->experience_id,
            'strong_leg' => $this->strong_leg,
            'field_position' =>
                [
                    'id' => $this->field_position->id ?? null,
                    'position' => $this->field_position->position ?? null,
                ],
            'preffered_position' => [
                'id' => $this->preffered_position->id ?? null,
                'position' => $this->preffered_position->position ?? null,
            ],
            'is_active' => $this->is_active,
            'languages' => $user->languages->pluck('language'),

            'height' => $this->height,
            'weight' => $this->weight,
            'gender' => [
                'id' => $this->gender->id ?? null,
                'name' => $this->gender->name ?? null],
            'intrested_in_foreign' => $this->intrested_in_foreign,
            'has_agent' => $this->has_agent,
            'has_contract' => $this->has_contract,
            'contract_date' => $this->contract_date,
            'about_me' => $this->about_me,
            'created_at' => $this->created_at

        ];
    }

}
