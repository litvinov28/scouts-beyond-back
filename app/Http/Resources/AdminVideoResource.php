<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminVideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

     //   $user = \App\User::withTrashed()->find($this->user_id);


        return [

            'id' => $this->id,
          //  'user_id' => $user->id,
            'path' => $this->path,
            'section' => $this->section,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}
