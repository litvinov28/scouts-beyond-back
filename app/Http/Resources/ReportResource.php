<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use function Zend\Diactoros\normalizeUploadedFiles;

class ReportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'my_id' => $this->my_id,
            'parent_name' => $this->parent_name,
            'parent_phone' => $this->parent_phone,
            'parent_email' => $this->parent_email,
            'player_type' => $this->player_type,
            'state' => $this->state,
            'player' => [
                'id' => $this->player->id ?? null,
                'user_id' => $this->player->user_id ?? null,
                'firstname' => $this->player->firstname ?? null,
                'lastname' => $this->player->lastname ?? null,
                'email' => $this->player->user->email ?? null,
            ],
            'coach' => [
                'id' => $this->user->id ?? null,
                'firstname' => $this->user->firstname ?? null,
                'lastname' => $this->user->lastname ?? null,
                'email' => $this->user->email ?? null,
            ],
            'comment' => $this->deleted_at ? $this->comment()->withTrashed()->get() : $this->comment,
            'rating' => $this->deleted_at ? $this->rating()->withTrashed()->get() : $this->rating,
            'status' => $this->status,
            'visibility' => $this->visibility,
            'due_date' => $this->due_date,
            'last_updated' => $this->last_updated,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
            'videos' => \App\Http\Resources\VideoResource::collection($this->videos)
        ];
    }
}