<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
        return [
            'id' => $this->id,
            'path' => $this->path,
            'section' => $this->section,
            'report_id' => $this->report_id,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ];
    }
}