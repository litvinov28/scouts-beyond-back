<?php

namespace App\Http\Helpers;

class FileFactory
{
    private $file;

    public function __construct(FileInterface $file)
    {
        $this->file = $file;
    }

    public function save(){
        return $this->file->save();
    }

    public function delete($name){
        return $this->file->delete($name);
    }
}