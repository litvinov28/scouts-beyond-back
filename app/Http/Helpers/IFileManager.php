<?php

namespace App\Helpers\Interfaces;

use Illuminate\Http\UploadedFile;

interface IFileManager
{
    /**
     * Save new file to storage.
     *
     * @param UploadedFile $tempFile
     * @param string $path
     * @return string
     */
    function saveFile(UploadedFile $tempFile, string $path);

    /**
     * Save new file in base64 format to storage.
     *
     * @param string $file
     * @param $path
     * @return string
     */
    public function saveBase64(string $file, string $path);

    /**
     * Remove file from storage.
     *
     * @param string $name
     * @param string $path
     * @return void
     */
    function deleteFile(string $name, string $path);
}
