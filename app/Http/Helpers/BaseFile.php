<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class BaseFile implements BaseFileInterface
{
    public $file;
    public $path;
    protected $fileName;

    public function __construct($file, $path)
    {
        $this->file = $file;
        $this->path = $path;
        $this->setFileName();
    }

    public function setFileName(){
        $this->fileName = 'file_'.time().'_'.str_random(8).'.'.$this->file->extension();
    }

    public function delete($name){
        if(!$name){
            $name = $this->file;
        }
        $saveImagePath = Config::get('constants.image_folder.addInfoImages.save_path');
        $saveVideoPath = Config::get('constants.video_folder.save_path');
        if(Storage::disk('local')->exists($saveImagePath.$name))
            $path = $saveImagePath;
        if(Storage::disk('local')->exists($saveVideoPath.$name))
            $path = $saveVideoPath;
        Storage::disk('local')->delete($path.$name);
    }
}