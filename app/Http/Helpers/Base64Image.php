<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Storage;

class Base64Image extends BaseFile implements FileInterface
{
    public function __construct($file, $path)
    {
        parent::__construct($file, $path);
    }

    public function save(): string
    {
        $avatar = $this->file;
        $path = $this->path;
        $fileName = $this->fileName;
        @list(, $avatar) = explode(';', $avatar);
        @list(, $avatar) = explode(',', $avatar);
        if($avatar != ""){
            Storage::disk('local')->put($path.$fileName, base64_decode($avatar));
        }
        return $fileName;
    }

    public function setFileName(){
        $this->fileName = 'image_'.time().'_'.str_random(8).'.png';
    }
}