<?php

namespace App\Http\Helpers;

interface FileInterface
{
    public function save() : string;
}