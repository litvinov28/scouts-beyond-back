<?php

namespace App\Http\Helpers;

interface BaseFileInterface
{
    public function delete($name);

    public function setFilename();
}