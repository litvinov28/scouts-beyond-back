<?php

namespace App\Http\Helpers;

class Video extends BaseFile implements FileInterface
{
    public function __construct($file, $path)
    {
        parent::__construct($file, $path);
    }

    public function save() : string {
        $video = $this->file;
        $fileName = $this->fileName;
        $path = $this->path;
        $video->storeAs($path, $fileName);
        return $fileName;
    }
}