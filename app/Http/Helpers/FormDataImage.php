<?php

namespace App\Http\Helpers;

use Illuminate\Support\Facades\Storage;

class FormDataImage extends BaseFile implements FileInterface
{
    public function __construct($file, $path)
    {
        parent::__construct($file, $path);
    }

    public function save() : string {
        $image = $this->file;
        $fileName = $this->fileName;
        $path = $this->path;
        $image->storeAs($path, $fileName);
        return $fileName;
    }

    public function setFileName(){
        $this->fileName = 'image_'.time().'_'.str_random(8).'.png';
    }
}