<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Http\Requests\VideoDeleteRequest;
use App\Report;
use App\Video;
use App\Http\Requests\VideoUpdateRequest;
use App\Http\Helpers\Video as VideoHelper;
use App\Http\Resources\VideoResource;
use Illuminate\Support\Facades\Config;
use App\Http\Resources\ReportResource;
class VideoController extends Controller
{
    public function update(VideoUpdateRequest $request)
    {
        $video = Video::find($request->video_id);
        $path = Config::get('constants.video_folder.reports.save_path');
      //  $newVideo = new VideoHelper($request->file, $path);
       // $filename = $newVideo->save();
        $updatedVideo = $video->update(['path' => $request->path]);
        return response()->json(['success' => $updatedVideo, 'message' => trans('messages.success.updatedStaticText'), 'data' => new VideoResource($video)]);
    }

    public function delete(VideoDeleteRequest $request, Video $video)
    {
        $request->only('id');
      $video =  Video::where('id', $request->id)->first();
      $report = Report::where('id', $video->report_id)->first();
      $video->delete();
         return response()->json(['success' => true, 'data' => new ReportResource($report)]);
    }
}
