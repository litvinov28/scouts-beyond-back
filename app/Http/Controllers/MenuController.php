<?php

namespace App\Http\Controllers;

use App\Http\Requests\MenuRequest;
use App\Http\Requests\MenuUpdateRequest;
use App\Key;
use App\Language;
use App\Menu;
use Illuminate\Http\Request;
use App\languageSite;
class MenuController extends Controller
{

    public function index(int $languageId)
    {

        $language_site = Menu::where('language_id',$languageId)->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }

    public function showByKey()
    {

        $language_site = Key::whereHas('menu')->with('menu')->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }


    public function store(MenuRequest $request)
    {
        $menu = new Menu($request->all());
        return response()->json(['success' => $menu->save(), 'data' => $menu]);
    }

    public function update(MenuUpdateRequest $request, Menu $menu)
    {
        return response()->json(['success' => $menu->update($request->only('name','location')),'data' => $menu]);
    }

    public function delete(Menu $menu)
    {
        return response()->json(['success' => $menu->delete(), 'data' => 'deleted']);
    }
}
