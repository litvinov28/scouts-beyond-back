<?php

namespace App\Http\Controllers\Auth;

use App\Http\Resources\UserResource;
use App\Role;
use App\User;
use App\Player;
use App\Http\Controllers\Controller;
use App\Http\Resources\PlayerResource;
use App\Http\Requests\LoginUserRequest;
use App\Http\Requests\SocialLoginUserRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Http\Request;
use Mail;
use App\VerifyUser;
use App\Mail\VerifyMail;
class LoginController extends Controller
{
    public function authenticate(LoginUserRequest $request)
    {
        $credentials = $request->only('email', 'password');
        $check = User::where('email', $credentials['email'])
            ->first();
        $social = User::where('email', $credentials['email'])->whereNotNull('social_id')
            ->first();
        if ($social){
            return response()->json(['success' => false, 'incorrect' => 'social', 'social_web' => $social->social]);
        }
        if (!$check) {
            return response()->json(['success' => false, 'incorrect' => 'email']);
        }
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['success' => false, 'incorrect' => 'password']);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $currentUser = User::getByEmail($credentials['email']);
        return response()->json(['success' => true, 'token' => $token, 'data' => new UserResource($currentUser)]);
    }

    public function socialLogin(SocialLoginUserRequest $request, User $users)
    {
        $newUser = false;
        try {
            if ($user = User::where('social_id', $request->social_id)->first()) {
                $token = JWTAuth::fromUser($user);
            } else {
                $newUser = true;
                $user = $users->createUser($request->all());
                $token = JWTAuth::fromUser($user);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        if($newUser) {
//            $verifyUser = VerifyUser::create([
//                'user_id' => $user->id,
//                'token' => str_random(40)
//            ]);
//            Mail::to($user->email)->send(new VerifyMail($user));
            Mail::send('mail.confirm-registration',  compact('user'), function ($message) use ($user) {
                $message->to($user->email)
                    ->subject('ברוכים הבאים!');
            });
            return response()->json(['type' => 'register' , 'success' => true, 'token' => $token, 'data' => new PlayerResource(Player::where('user_id', $user->id)->first())]);
        }
        return response()->json(['type'=> 'login','success' => true, 'token' => $token, 'data' =>$user]);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $check = User::where('email', $credentials['email'])
            ->filterRole(['Coach','Admin'])
            ->first();
        if (!$check) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.notExist')]);
        }

        if (!Hash::check($credentials['password'], $check->password)) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.invalidPassword')]);
        }

        $user = User::getByEmail($credentials['email']);
        $token = null;
        try {
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['success' => false, 'message' => trans('messages.errors.cantFindAccount')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.failedLogin')], 500);
        }
        return response()->json(['success' => $user, 'token' => $token]);
    }
}
