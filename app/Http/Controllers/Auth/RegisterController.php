<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Http\Resources\UserResource;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyMail;
use App\VerifyUser;
class RegisterController extends Controller
{
    public function register(RegisterUserRequest $request, User $user){
        $token = null;
        $credentials = $request->all();
        try {
            $user = $user->createUser($credentials);
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['success' => false, 'message' => trans('messages.errors.cantFindAccount')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.failedLogin')], 500);
        }



//
//        $verifyUser = VerifyUser::create([
//            'user_id' => $user->id,
//            'token' => str_random(40)
//        ]);
       switch ($user->language_site_id){
           case 1:
               Mail::send('mail.confirm-registration_eng',  compact('user'), function ($message) use ($user) {
                   $message->to($user->email)
                       ->subject('ברוכים הבאים!');
               });
               break;
           case 2:
               Mail::send('mail.confirm-registration',  compact('user'), function ($message) use ($user) {
                   $message->to($user->email)
                       ->subject('ברוכים הבאים!');
               });
               break;
       }
//        Mail::send('mail.confirm-registration',  compact('user'), function ($message) use ($user) {
//            $message->to($user->email)
//                ->subject('ברוכים הבאים!');
//        });
    //    Mail::to($user->email)->send(new VerifyMail($user));

        return response()->json(['success' => true, 'message' => trans('messages.success.registeredUser'), 'user' => new UserResource($user), 'token' => $token]);
    }

    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if(!$user->verified) {
                $verifyUser->user->verified = 1;
                $verifyUser->user->save();

                Mail::send('mail.confirm-registration',  compact('user'), function ($message) use ($user) {
                    $message->to($user->email)
                        ->subject('ברוכים הבאים!');
                });

                return redirect('/?success=1');
            }
        }


        return redirect('/?success=0');
    }
}
