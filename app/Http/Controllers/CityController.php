<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function index(City $city)
    {
        return response()->json([
            'success'=> $city::select('id','name','country_id')->orderBy('name')->get()
        ]);
    }

//    public function import(Request $request)
//    {
//        $cities = $request->all();
//        foreach ($cities as $city) {
//            $name = $city['properties']['name'];
//            $lat = round($city['geometry']['coordinates'][0], 6);
//            $lng = round($city['geometry']['coordinates'][1], 6);
//            $old = City::where([
//                'latitude' => $lat,
//                'longitude' => $lng
//            ])->first();
//            if (!$old) {
//                City::create([
//                    'name' => $name,
//                    'latitude' => $lat,
//                    'longitude' => $lng
//                ]);
//            }
//        }
//        return response()->json(true);
//    }

    public function filterCountry(Country $country)
    {
        $cities = City::where('country_id',$country->id)->get();
        return response()->json(['success' => true,'data' => $cities]);

    }
}
