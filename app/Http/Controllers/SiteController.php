<?php

namespace App\Http\Controllers;

class SiteController extends Controller
{
    public function getAmountOfPCR(){
        $data['players'] = \App\Player::all()->count();
        $data['coaches'] = \App\Coach::all()->count();
        $data['reports'] = \App\Report::all()->count();
        return response()->json(['success' => true, 'data' => $data]);
    }
    
}
