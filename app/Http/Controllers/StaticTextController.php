<?php

namespace App\Http\Controllers;

use App\Http\Requests\StaticTextDeleteRequest;
use App\Http\Requests\StaticTextStoreRequest;
use App\Http\Requests\StaticTextUpdateRequest;
use App\StaticText;

class StaticTextController extends Controller
{
    public function index(int $language)
    {
        $language_site = StaticText::where('language_id', $language)->get();
        return response()->json($language_site);

        

//        $all = StaticText::all();
//        return response()->json(['success' => true, 'data' => $all]);
    }

    public function store(StaticTextStoreRequest $request){
        $staticText = StaticText::create($request->all());
        return response()->json(['success' => true, 'message' => trans('messages.success.createdStaticText'), 'data' => $staticText]);
    }

    public function update(StaticTextUpdateRequest $request){
        $credentials = $request->only(['title', 'description','subtitle']);
        $staticText = StaticText::find($request->static_text_id);
        $staticText->update(['title' => $credentials['title'], 'description' => $credentials['description'], 'subtitle' => $credentials['subtitle']]);
        return response()->json(['success' => true, 'message' => trans('messages.success.updatedStaticText'), 'data' => $staticText]);
    }

    public function delete(StaticTextDeleteRequest $request){
        $credentials = $request->only('ids');
        StaticText::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => trans('messages.success.deletedStaticText')]);
    }

    public function getStaticText($title){
        try {
            $staticText = StaticText::where('title', $title)->firstOrFail();
            return response()->json(['success' => true, 'data' => $staticText]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

