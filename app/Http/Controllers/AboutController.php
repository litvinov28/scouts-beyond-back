<?php

namespace App\Http\Controllers;

use App\Http\Requests\AboutRequest;
use App\Http\Traits\PhotoTrait;
use App\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\About;

class AboutController extends Controller
{
    use PhotoTrait;

    public function index(int $language)
    {
        $language_site = About::where('language_id', $language)->get();
        return response()->json($language_site);
    }

    public function showByKey()
    {
        $keys = Key::whereHas('aboutUs')->with('aboutUs')->get();
        return response()->json(['success' => true,'data' => $keys]);
    }

    public function indexAll()
    {
        return response()->json(['success' => About::all()]);
    }


    public function store(AboutRequest $request)
    {
        $about = new About($request->all());


      $path = config('constants.about_folder.about.save_path');

        $about->photo = $this->savePhoto($request->photo, $path);

        return response()->json(['success' => $about->save(), 'data' => $about]);
    }

    public function update(Request $request, About $about)
    {
        $path = config('constants.about_folder.about.save_path');
        if ($request->has('avatar')) {
            if (!is_null($about->photo)) {
                $this->deletePhoto($about->photo, $path);
            }
            $about->photo =  $this->savePhoto($request['photo'], $path);
        }
        return response()->json(['success' => $about->update($request->except(['photo'])), 'data' => $about]);

    }

    public function delete(About $about)
    {
        return response()->json(['success' => $about->delete(), 'message' => 'deleted']);
    }
}
