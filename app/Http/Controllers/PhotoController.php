<?php

namespace App\Http\Controllers;

use App\Http\Traits\PhotoTrait;
use App\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{

    use PhotoTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Photo $photo)
    {
        return response()->json(['success' => true, 'data' => $photo->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $mail = new Photo($request->all());
        $path = config('constants.file_mail_photo.mail_photo.save_path');
        $mail->name = $this->savePhoto($request->name, $path);
        return response()->json(['success' => $mail->save(), 'data' => $mail]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
        $path = config('constants.file_mail_photo.mail_photo.save_path');
        if($request->has('name'))
        {
            if(!is_null($photo->name))
            {
                $this->deletePhoto($photo->getOriginal('name'), $path);
            }
            $photo->name = $this->savePhoto($request['name'], $path);
        }
        return response()->json(['success' => $photo->update($request->except(['name'])), 'data' => $photo]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        return response()->json(['success' => $photo->delete(), 'message' => 'deleted']);

    }
}
