<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubjectRequest;
use App\Key;
use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
    public function index(int $language)
    {
        $language_site = Subject::where('language_id', $language)->get();
        return response()->json(['success' => $language_site]);
//        return response()->json(['success' => Subject::all()]);
    }

    public function update(Subject $subject, SubjectRequest $request)
    {
        return response()->json(['success' => $subject->update($request->all()), 'data' => $subject]);
    }

    public function store(SubjectRequest $request)
    {
        $subject = new Subject($request->all());
        return response()->json(['success' => $subject->save(), 'data' => $subject]);
    }

    public function delete(Subject $subject)
    {
     return response()->json(['success' => $subject->delete() , 'message' => 'deleted']);
    }

    public function showByKey()
    {
        $language_site = Key::whereHas('subject')->with('subject')->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }
}
