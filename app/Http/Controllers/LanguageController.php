<?php

namespace App\Http\Controllers;

use App\Http\Requests\LanguageRequest;
use App\Key;
use App\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    public function index(int $languageId)
    {

        $language_site = Language::where('language_id',$languageId)->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }

    public function showByKey()
    {

        $language_site = Key::whereHas('languages')->with('languages')->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }

    public function store(LanguageRequest $request)
    {
        $language = new Language($request->all());
        return response()->json(['success' => $language->save(), 'data' => $language]);
    }

    public function update(LanguageRequest $request, Language $language)
    {
        return response()->json(['success' => $language->update($request->all()),'data' => $language]);
    }

    public function destroy(Language $language)
    {
     return response()->json(['success' => $language->delete(),'message'=> 'deleted']);
    }
}
