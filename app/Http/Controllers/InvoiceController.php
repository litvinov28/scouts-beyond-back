<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvoiceRequest;
use App\Invoice;
use Illuminate\Http\Request;

class InvoiceController extends Controller
{
    public function store(InvoiceRequest $request)
    {
        $invoices = Invoice::updateOrCreate([
            'report_id' => $request->input('report_id')
        ], $request->all());
        return response()->json(['success' => true, 'data' => $invoices]);
    }
}
