<?php

namespace App\Http\Controllers;

use App\Key;
use App\Player;
use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\JWT;

class PositionController extends Controller
{
    public function index(Position $position)
    {
        return response()->json(['success' => $position::all()]);
    }

public function indexLanguage(int $language)
{
    $language_site = Position::where('language_id', $language)->get();
    return response()->json($language_site);
}

    public function getPosition(Position $position, Request $request)
    {
        $test = new Carbon(Player::min('date_of_birth'));
        $age = now()->subYears($test->year)->year;
        $position1 = Player::groupBy('field_position_id')
            ->select(
                DB::raw('count(*) as count'),
                'field_position_id'
            );

        $token = JWTAuth::getToken();
        $validateToken = JWTAuth::check($token);
        if ($validateToken !== false) {
            $currentUser = JWTAuth::parseToken()->authenticate();
            $position1->where('user_id','<>', $currentUser->id);
        }

        $positions = $position1->get();

        return response()->json([
            'success'=> $positions,
            'max_age' => $age,
        ]);
    }

    public function showByKey()
    {

        $key = Key::whereHas('position')->with('position')->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $key]);
    }
}
