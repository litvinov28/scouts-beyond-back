<?php

namespace App\Http\Controllers;

use App\Mail;
use Illuminate\Http\Request;
use App\Key;
class MailController extends Controller
{
    public function store(Request $request)
    {
     $mail = new Mail($request->all());
     return response()->json(['success' => $mail->save(),'data' => $mail]);
    }
    public function update(Mail $mail, Request $request)
    {

        return response()->json(['success' =>   $mail->update($request->only('text','subject')),'data'=> $mail]);
    }


    public function show(Mail $mail){
        return response()->json(['success' => true,'data' =>$mail]);
    }
    public function destroy(Mail $mail){
        return response()->json(['success' => $mail->delete()]);
    }

    public function index(int $language)
    {
        $language_site = Mail::where('language_id', $language)->get();
        return response()->json($language_site);
    }

    public function showByKey()
    {
        $keys = Key::whereHas('mail')->with('mail')->get();
        return response()->json(['success' => true,'data' => $keys]);
    }
}
