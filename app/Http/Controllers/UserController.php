<?php

namespace App\Http\Controllers;

use App\Helpers\Interfaces\IFileManager;
use App\Http\Requests\CoahRequest;
use App\Http\Requests\DeleteCoachRequest;
use App\Http\Requests\DeleteUsersRequest;
use App\Http\Requests\DeleteScoutRequest;
use App\Http\Requests\PasswordRequest;
use App\Http\Requests\RestoreRequest;
use App\Http\Requests\ScoutCreateRequest;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UpdateCoachRequest;
use App\Http\Requests\UpdatePlayerRequest;
use App\Http\Requests\UserVideoRequest;
use App\Http\Resources\ReportResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\AdminVideoResource;
use App\Report;
use App\User;
use App\UserVideo;
use Hash;
use App\Player;
use App\Http\Resources\PlayerResource;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateUserPhotoRequest;
use App\Http\Requests\UpdateUserPasswordRequest;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\Null_;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Helpers\FormDataImage;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Carbon;
use App\Http\Helpers\Video as VideoHelper;
use Illuminate\Support\Facades\Mail;
use Vimeo\Laravel\Facades\Vimeo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use App\Mail as Email;

class UserController extends Controller
{

    public function index()
    {
        $players = Player::all()
            ->where('experience_id', '>', '0')
            ->where('date_of_birth', '>', '0')
            ->where('country_id', '>', '0')
            ->where('field_position_id', '>', '0')
            ->where('gender_id', '>', '0');
        return response()->json(['success' => true, 'data' => PlayerResource::collection($players)]);
    }

    public function getMe()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        return response()->json(['success' => true, 'data' => new PlayerResource(Player::getByUser($currentUser))]);
    }

//    public function update(UpdateUserRequest $request, User $user)
//    {
//        $credentials = $request->all();
//        $updatedUser = $user->updateUser($credentials);
//        return response()->json(['success' => true, 'message' => trans('messages.success.updatedUser'), 'data' => new PlayerResource(Player::getByUser($updatedUser))]);
//    }

    public function update(UpdateUserRequest $request)
    {
        $user = JWTAuth::user();
        $credentials = $request->all();
        $path = config('constants.file_folder.resume.save_path');
        $file = $request->file('resume');
        if ($file) {
            Storage::delete($path . $user->player->getOriginal('resume'));

            $filename = 'resume' . time() . '.pdf';
            $resume = Storage::putFileAs($path, new File($file), $filename);
            $user->player()->update(['resume' => $filename]);
        }


        $updatedUser = $user->updateUser($credentials);
        return response()->json(['success' => true, 'message' => trans('messages.success.updatedUser'), 'data' => new PlayerResource(Player::getByUser($updatedUser))]);
    }

    public function updatePhoto(UpdateUserPhotoRequest $request)
    {
        $player = JWTAuth::user()->player;
        $path = Config::get('constants.image_folder.avatars.save_path');
        $photo = new FormDataImage($request->file, $path);
        $filename = $photo->save();
        $updatedPlayer = $player->update(['avatar_src' => $filename]);
        return response()->json(['success' => $updatedPlayer, 'message' => trans('messages.success.updatedUser'), 'data' => new PlayerResource($player)]);
    }

    public function updatePassword(UpdateUserPasswordRequest $request)
    {
        $currentUser = JWTAuth::user();
        if (Hash::check($request->get('password'), $currentUser->password)) {
            $currentUser->update(['password' => Hash::make($request->get('new_password'))]);
            return response()->json(['success' => true, 'message' => 'Password updated']);
        }
        return response()->json(['success' => false, 'error' => 'Old password is incorrect']);
    }

    public function delete()
    {
        $currentUser = JWTAuth::user();
        $player = Player::getByUser($currentUser);
        $currentUser->delete();
        $player->delete();

        $mail = Email::where('language_id', $currentUser->language_site_id)->where('key_id', '40')->first();

        switch ($currentUser->language_site_id) {
            case 1:
                $body = str_replace('$firstname', $currentUser->firstname, $mail->text);
                Mail::send('mail.user-delete-user-confirm', compact('body'), function ($message) use ($currentUser, $mail) {
                    $message->to($currentUser->email)
                        ->subject($mail->subject);
                });
                break;
            case 2:
                $body = str_replace('$firstname', $currentUser->firstname, $mail->text);
                Mail::send('mail.user-delete-user-confirm', compact('body'), function ($message) use ($currentUser, $mail) {
                    $message->to($currentUser->email)
                        ->subject($mail->subject);
                });
                break;

        }


        Mail::send('mail.delete-user-send-to-admin', compact('currentUser'), function ($message) use ($currentUser) {
            $message->to('info@scoutbeyond.com')
                ->subject('אישור מחיקת הפרופיל באתר');
        });


        return response()->json(['success' => true, 'message' => 'Account was successfully deleted']);
    }

    public function search(SearchRequest $request, Player $player)
    {
        $search = Player::ByExperience($request->experience_id)
            ->byPosition($request->field_position_id)
            ->byGender($request->gender_id)
            ->ByCountry($request->country_id)
            ->whereDate('date_of_birth', '<', Carbon::today()->subYears($request->min_range)->toDateString())
            ->whereDate('date_of_birth', '>', Carbon::today()->subYears($request->max_range + 1)->toDateString())
            ->get();
        return response()->json(PlayerResource::collection($search));
    }

    public function loadVideo(UserVideoRequest $request)
    {
        $currentUser = JWTAuth::user();
        $path = Config::get('constants.uservideo_folder.uservideo.save_path');
        $gpath = Config::get('constants.uservideo_folder.uservideo.get_path');
        $getPath = url($gpath);
        foreach ($request->section as $numOfSection => $videos) {
            foreach ($videos as $video) {
//                $video = new VideoHelper($video, $path);
//                $filename = $video->save();
//                $filePath = storage_path('app/' . $path . $filename);
//                $vimeoPath = Vimeo::upload($filePath);
//
//                $videoId = str_replace('/videos/', '', $vimeoPath);
//                Storage::delete($path . $filename);

                $create = UserVideo::create([
                    'section' => $numOfSection,
                    'path' => $video,
                    'user_id' => $currentUser->id]);
            }
        }
        return response()->json(['success' => true, 'message' => 'Video was successfully created', 'data' => $create]);
    }

    public function getVideo(UserVideo $userVideo)
    {
        return $userVideo::all();
    }

    public function createCoach(CoahRequest $request, User $user)
    {
        $credentials = $request->all();
        $user = $user->createCoach($credentials);
        return response()->json(['success' => 'coach', 'data' => $user]);
    }

    public function updateCoach(UpdateCoachRequest $request)
    {
        $credentials = $request->only(['firstname', 'lastname', 'email', 'password']);
        $staticText = User::find($request->coach_id);
        $staticText->update(['firstname' => $credentials['firstname'], 'lastname' => $credentials['lastname'], 'email' => $credentials['email'], 'password' => Hash::make($credentials['password'])]);
        return response()->json(['success' => true, 'message' => trans('messages.success.updatedCoach'), 'data' => $staticText]);
    }

    public function getCoach()
    {
        $coach = User::where('role_id', '2')->get();
        return $coach;
    }

    public function deleteCoach(DeleteCoachRequest $request)
    {
        $credentials = $request->only('ids');
        User::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => 'Coach has been deleted']);
    }

    public function deleteUsers(DeleteUsersRequest $request)
    {
        $credentials = $request->only('ids');
        User::withTrashed()->whereIn('id', $credentials['ids'])->get()->map(function ($user) {
            $user->player->delete();
            $user->delete();
        });

        return response()->json(['success' => true, 'message' => 'Users has been deleted']);
    }

    public function myreport()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        return response()->json([
            'success' => true,
            'data' => ReportResource::collection(Report::getByUser($currentUser)->get()),
        ]);

    }


    public function reportPlayer()
    {
        $currentUser = JWTAuth::user()->player;
        return response()->json([
            'success' => true,
            'data' => ReportResource::collection(Report::getByPlayer($currentUser)->get()),
        ]);

    }

    public function deleteVideo(UserVideo $userVideo)
    {
        return response()->json(['success' => $userVideo->delete(), 'message' => 'deleted']);
    }

    public function getId(User $user)
    {
        return new PlayerResource($user->player);
    }


    public function reset(Request $request, User $user)
    {

        $email = User::where('email', $request->email)->first();
        if (!$email) {
            return response()->json(['success' => false]);
        }
        $pass = str_random(8);
        $email->password = Hash::make($pass);
        $email->update($request->only('password'));

        $mail = Email::where('language_id', $email->language_site_id)->where('key_id', 42)->first();

        switch ($email->language_site_id) {
            case 1:
                $body = str_replace(['$firstname','$pass'],[$email->firstname,$pass], $mail->text);
                Mail::send('mail.send-temp-password', compact('body'), function ($message) use ($request,$mail) {
                    $message->to($request->email)
                        ->subject($mail->subject);
                });
                break;
            case 2:
                $body = str_replace(['$firstname','$pass'],[$email->firstname,$pass], $mail->text);
                Mail::send('mail.send-temp-password', compact('body'), function ($message) use ($request,$mail) {
                    $message->to($request->email)
                        ->subject($mail->subject);
                });
                break;

        }
        return response()->json(['success' => $email]);


    }

    public function all()
    {
        $players = Player::all();
        return response()->json([
            'success' => true,
            'data' => PlayerResource::collection($players),
        ]);
    }

    public function updatePlayer(UpdatePlayerRequest $request)
    {
        $credentials = $request->all();
        $user = User::find($request->user_id);
        $updatedUser = $user->updateUser($credentials);
        return response()->json(['success' => true, 'message' => trans('messages.success.updatedUser'), 'data' => new PlayerResource(Player::getByUser($updatedUser))]);
    }

    public function password(PasswordRequest $request)
    {
        $user = User::find($request->user_id);
        $user->email;
        $name = $user->firstname;
        $pass = $request->password;
        $hash = $user->password = Hash::make($pass);
        $user->update([$hash]);

        $mail = Email::where('language_id', $user->language_site_id)->where('key_id', 43)->first();

        switch ($user->language_site_id) {
            case 1:
                $body = str_replace(['$name','$pass'],[$user->firstname,$pass], $mail->text);
                Mail::send('mail.send-admin-password', compact('body'), function ($message) use ($user,$mail) {
                    $message->to($user->email)
                        ->subject($mail->subject);
                });
                break;
            case 2:
                $body = str_replace(['$name','$pass'],[$user->firstname,$pass], $mail->text);
                Mail::send('mail.send-admin-password', compact('body'), function ($message) use ($user,$mail) {
                    $message->to($user->email)
                        ->subject($mail->subject);
                });
                break;

        }
        return response()->json(['success' => $user]);

    }

    public function listDelete()
    {

        $players = Player::onlyTrashed()->get();
        return response()->json(['success' => true, 'data' => PlayerResource::collection($players)]);

    }

    public function DeletedCoach()
    {
        $coach = User::onlyTrashed()->where('role_id', '2')->get();
        return response()->json(['success' => true, 'data' => $coach]);
    }

    public function restore(RestoreRequest $request)
    {
        $users = User::onlyTrashed()->findOrFail($request->user_id);
        $player = $users->player()->withTrashed()->first();
        $users->restore();
        if ($player == null) {
            return response()->json(['success' => true]);
        } else {
            $player->restore();
        }

        return response()->json(new PlayerResource($player));
    }

    public function createScout(ScoutCreateRequest $request)
    {
        $scout = new User($request->all());
        $scout->role_id = 4;
        $scout->password = Hash::make($request['password']);

        return response()->json(['success' => $scout->save(), 'data' => $scout]);
    }

    public function updateLanguage(Request $request)
    {
        $user = auth()->user();
        return response()->json(['success' => $user->update($request->only('language_site_id')),'data' => $user]);
    }

    public function scoutList()
    {
        $scout = User::where('role_id',4)->get();
        return response()->json(['success' => true,'data' => $scout]);
    }

    public function DeletedScout()
    {
        $scout = User::onlyTrashed()->where('role_id', '4')->get();
        return response()->json(['success' => true, 'data' => $scout]);
    }

    public function deleteScout(DeleteScoutRequest $request)
    {
        $credentials = $request->only('ids');
        User::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => 'Scout has been deleted']);
    }
}
