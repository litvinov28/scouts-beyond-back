<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentRequest;
use App\Http\Requests\PaymentUpdateRequest;
use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function store(Payment $payment, PaymentRequest $request)
    {
        $payment = new Payment($request->all());
        return response()->json(['success' => $payment->save(), 'data' => $payment]);
    }

    public function index(Payment $payment)
    {
        return response()->json(['success' => $payment::all()]);
    }

    public function update(Payment $payment,PaymentUpdateRequest $request)
    {
        $credentials = $request->all();
        $payment = Payment::find($request->id);
        $payment->update($credentials);
        return response()->json(['success' => true, 'data' => $payment]);

    }
}
