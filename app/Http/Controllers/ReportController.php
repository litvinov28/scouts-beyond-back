<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\AddVideoRequest;
use App\Http\Requests\ReportCoachRequest;
use App\Http\Requests\ReportDeleteAdminRequest;
use App\Http\Requests\RestoreReportRequest;
use App\Http\Requests\VisibilityRequest;
use App\Invoice;
use App\Rating;
use App\Report;
use App\Player;
use App\User;
use App\Video;
use App\Http\Helpers\Video as VideoHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Config;
use App\Http\Requests\ReportStoreRequest;
use App\Http\Requests\ReportUpdateRequest;
use App\Http\Resources\ReportResource;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Vimeo\Laravel\Facades\Vimeo;
use App\Mail as Email;
class ReportController extends Controller
{
    public function getAll(Report $report)
    {
        $all = $report::all();
        return response()->json(['success' => true, 'data' => ReportResource::collection($all)]);
    }

    public function index()
    {
        $all = Player::where('user_id', JWTAuth::user()->id)->firstOrFail();
        return response()->json(['success' => true, 'data' => ReportResource::collection($all->reports->load('videos'))]);
    }

    public function store(ReportStoreRequest $request, Report $report)
    {
        $credentials = $request->except('section');
        $player = Player::where('user_id', JWTAuth::user()->id)->first();
        $path = Config::get('constants.video_folder.reports.save_path');

        $max = Report::withTrashed()->where('player_id', $player->id)->max('my_id');
        $report = Report::create(array_merge($credentials, [
            'player_id' => $player->id,
            'my_id' => $max + 1,
            'due_date' => Carbon::now()->addDays(7),
        ]));
        foreach ($request->section as $numOfSection => $videos) {
            foreach ($videos as $video) {
//                dd($video);
//
//                $video = new VideoHelper($video, $path);
   //             $filename = $video->save();
//
//                $filePath = storage_path('app/' . $path . $filename);
//                $vimeoPath = Vimeo::upload($filePath);
//
//                $videoId = str_replace('/videos/', '', $vimeoPath);
//                Storage::delete($path . $filename);

                Video::create([
                    'section' => $numOfSection,
                    'path' => $video,
                    'report_id' => $report->id
                ]);
            }

        }




        if($request->input('state') == 'published' ) {
            $email = $report->player->user->email;
            Mail::send('mail.report-admin', compact('report', 'email'), function ($message) use ($email) {
                $message->to('info@scoutbeyond.com')
                    ->subject('report');
            });
        }



        return response()->json([
            'success' => true,
            'message' => 'Report was successfully created',
            'data' => new ReportResource($report->load('videos'))
        ]);
    }

    public function update(ReportUpdateRequest $request)
    {
        $credentials = $request->except('section');
        $report = Report::find($request->report_id);
        $report->update($credentials);




        $email = $report->player->user->email;
        if($request->input('state' == 'published')) {
            Mail::send('mail.report-admin', compact('report', 'email'), function ($message) use ($email) {
                $message->to('info@scoutbeyond.com')
                    ->subject('report');
            });
        }
        return response()->json([
            'success' => true,
            'message' => trans('messages.success.updatedStaticText'),
            'data' => new ReportResource($report->load('videos')),
        ]);
    }

    public function addVideo(AddVideoRequest $request)
    {
        $credentials = $request->except('section');
        $report = Report::find($request->report_id);
////        return response()->json(['success' => true, 'message' => trans('messages.success.updatedStaticText'), 'data' => new ReportResource($report->load('videos'))]);
//        $credentials = $request->except('section');
//        $player = Player::where('user_id', JWTAuth::user()->id)->first();
//        $report = Report::create(array_merge($credentials, ['player_id' => $player->id]));


        $path = Config::get('constants.video_folder.reports.save_path');
        foreach ($request->section as $numOfSection => $videos) {
            foreach ($videos as $video) {
//                $video = new VideoHelper($video, $path);
//                $filename = $video->save();
//                $filePath = storage_path('app/' . $path . $filename);
//                $vimeoPath = Vimeo::upload($filePath);
//
//                $videoId = str_replace('/videos/', '', $vimeoPath);
//                Storage::delete($path . $filename);
                Video::create([
                    'section' => $numOfSection,
                    'path' => $video,
                    'report_id' => $report->id
                ]);
            }

        }
        return response()->json(['success' => $report->update($credentials), 'message' => 'Report was successfully created', 'data' => new ReportResource($report->load('videos'))]);
    }


    public function show(Report $report)
    {
        return response()->json(['success' => true, 'data' => new ReportResource($report->load('videos'))]);
    }

    public function updateByAdmin(Report $report, ReportCoachRequest $request)
    {
        $credentials = $request->only('coach_id', 'status', 'id');
        $report = Report::whereIn('id', $credentials['id'])->update(
            [
                'coach_id' => $credentials['coach_id'],
                'status' => $credentials['status']
            ]
        );
        $emailCoach = Report::where('coach_id', $credentials['coach_id'])->first()->user->email;
        $LangCoach = Report::where('coach_id', $credentials['coach_id'])->first()->user->language_site_id;

//        $users = User::query()
//            ->leftJoin('players', 'players.user_id', '=', 'users.id')
//            ->leftJoin('reports', 'players.id', '=', 'reports.player_id')
//            ->whereIn('reports.id', $credentials['id'])
//            ->select(['users.email', 'reports.id'])
//            ->get()->toArray();
//
//        foreach ($users as $user) {
//            Mail::send('mail.repot-prepared-coach', compact('user'), function ($message) use ($user) {
//                $message->to($user['email'])
//                    ->subject('דו"ח מאמן חדש נכנס למערכת');
//            });
//        }
$mail = Email::where('language_id',$LangCoach)->where('key_id','41')->first();


switch ($LangCoach){
    case 1:
        foreach($credentials['id'] as $value) {
            $body = str_replace('$value', $value, $mail->text);
            Mail::send('mail.trainer-report-sent-successfully', compact('body'), function ($message) use ($emailCoach, $mail) {
                $message->to($emailCoach)
                    ->subject($mail->subject);
            });
        }
        break;
    case 2:
        foreach($credentials['id'] as $value) {
            $body = str_replace('$value', $value, $mail->text);
            Mail::send('mail.trainer-report-sent-successfully', compact('body'), function ($message) use ($emailCoach, $mail) {
                $message->to($emailCoach)
                    ->subject($mail->subject);
            });
        }
        break;
}


//        Mail::send('mail.repot-prepared-coach', compact('users'), function ($message) use ($users) {
//
//            $message->to($users->pluck('email')->toArray())
//
//                ->subject('report');
//        });
        return response()->json(['success' => true, 'message' => 'updated', 'data' => $report]);
    }

    public function updateVisibility(Report $report, VisibilityRequest $request)
    {

        return response()->json(['success' => $report->update($request->only('visibility')), 'data' => new ReportResource($report)]);

    }

    public function getReport()
    {
        $player = JWTAuth::user()->player;


        $draft = Report::where('state', 'draft')->where('player_id', $player->id)->get()->count();
        $myreport = Report::where('player_id', $player->id)->get()->count();
        if ($myreport == $draft) {
            return response()->json(['success' => true, 'message' => '1-']);

        } else {
            if ($myreport >= $draft) {
                return response()->json(['success' => false, 'message' => 'you have a 1+ reports']);
            }
        }

    }

    public function delete(ReportDeleteAdminRequest $request)
    {
        $credentials = $request->only('ids');
        $report = Report::whereIn('id', $credentials['ids'])->delete();
       $rating = Rating::GetByReport($credentials['ids']);
       $rating->delete();
       $comment = Comment::GetByReport($credentials['ids']);
       $comment->delete();
        return response()->json(['success' => true, 'message' => trans('messages.success.deleted')]);
    }

    public function getStatus()
    {
        $player = JWTAuth::user()->player;
        $status = Report::where('player_id', $player->id)->where('status', '2')->get()->count();
        if ($status >= 1) {
            return response()->json(['success' => true, 'message' => 'You have a status 2']);
        } else
            return response()->json(['success' => false]);
    }


    public function listDelete()
    {
        $reports = Report::onlyTrashed()->get();
//        $rating = Rating::onlyTrashed()->get();
//        $comment = Comment::onlyTrashed()->get();
        return response()->json(['success' => true, 'data' => ReportResource::collection($reports)]);

    }

    public function restore(RestoreReportRequest $request)
    {
        $report = Report::onlyTrashed()->findOrFail($request->report_id);
        $comment = $report->comment()->withTrashed()->first();
        $rating = $report->rating()->withTrashed()->first();
        $report->restore();

        if ($rating == null){
            return response()->json(['success' => true,'data' => $report]);
        }else{
            $rating->restore();
        }
        if ($comment == null){
            return response()->json(['success' => true,'data' => $report]);
        }else{
            $comment->restore();
        }

        return response()->json(['success' => true,'data' => $report]);
    }

    public function showDoneReport(Player $player)
    {
        $reports = Report::where('player_id',$player->id)->where('status','2')->get();
        return response()->json(['success' => true, 'data' => ReportResource::collection($reports)]);
    }
}
