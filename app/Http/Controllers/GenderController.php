<?php

namespace App\Http\Controllers;

use App\Gender;
use App\Key;
use Illuminate\Http\Request;

class GenderController extends Controller
{


    public function index(int $language)
    {
        $language_site = Gender::where('language_id', $language)->get();
        return response()->json(['success' => $language_site]);
//        return response()->json(['success' => $gender::all()]);
    }

    public function showByKey()
    {

        $language_site = Key::whereHas('gender')->with('gender')->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }
}
