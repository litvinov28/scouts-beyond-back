<?php

namespace App\Http\Controllers;

use App\DynamicText;
use App\Http\Requests\DynamicTextDeleteRequest;
use App\Http\Requests\DynamicTextRequest;
use App\Http\Requests\DynamicTextUpdateRequest;
use Illuminate\Http\Request;

class DynamicTextController extends Controller
{
    public function index(int $language)
    {

        $language_site = DynamicText::where('language_id', $language)->get();
//        return response()->json($language_site);
//        $all = DynamicText::all();
        return response()->json(['success' => true, 'data' => $language_site]);
    }

    public function indexShow()
    {
//        dd('123');
//        $language_site = DynamicText::where('language_id')->get();
//        $language_site = DynamicText::where('language_id', $language)->get();
//        return response()->json($language_site);
        $all = DynamicText::all();
//        dd($all);
        return response()->json(['success' => true, 'data' => $all]);
    }

    public function store(DynamicTextRequest $request){
        $staticText = DynamicText::create($request->all());
        return response()->json(['success' => true, 'message' => trans('messages.success.createdStaticText'), 'data' => $staticText]);
    }

    public function update(DynamicTextUpdateRequest $request){
        $credentials = $request->all();
        $staticText = DynamicText::find($request->dynamic_text_id);
        $staticText->update($credentials);
        return response()->json(['success' => true, 'message' => trans('messages.success.updatedStaticText'), 'data' => $staticText]);
    }

    public function delete(DynamicTextDeleteRequest $request){
        $credentials = $request->only('ids');
        DynamicText::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => trans('messages.success.deletedStaticText')]);
    }

    public function getDynamicText($title){
        try {
            $staticText = DynamicText::where('title', $title)->firstOrFail();
            return response()->json(['success' => true, 'data' => $staticText]);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
