<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileButtonRequest;
use App\ProfileButton;
use Illuminate\Http\Request;
use App\Key;
class ProfileButtonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $language)
    {
        //
        $language_site = ProfileButton::where('language_id',$language)->get();

        return response()->json(['success' => $language_site]);

    }

    public function showByKey()
    {
        $keys = Key::whereHas('buttons')->with('buttons')->get();
        return response()->json(['success' => true,'data' => $keys]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProfileButtonRequest $request)
//    public function store(ProfileButton $profileButton)
    {
        //
//        dd($request);
        $profileButtonSave = new ProfileButton($request->all());
        return response()->json(['success' => $profileButtonSave->save(), 'data' => $profileButtonSave]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProfileButton  $profileButton
     * @return \Illuminate\Http\Response
     */
    public function show(ProfileButton $profileButton)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProfileButton  $profileButton
     * @return \Illuminate\Http\Response
     */
    public function edit(ProfileButton $profileButton)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProfileButton  $profileButton
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileButton $button, Request $request)
    {
        return response()->json(['success' => $button->update($request->only('name')),'data' => $button],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProfileButton  $profileButton
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProfileButton $profileButton)
    {
        //
        return response()->json(['success' => $profileButton->delete(),'message'=> 'deleted']);
    }
}
