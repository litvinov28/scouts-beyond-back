<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsStoreRequest;
use App\ContactUS;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
//    public function index(ContactUS $model)
    public function index(int $language)
    {
        $language_site = ContactUS::where('language_id', $language)->get();
//        dd($language);
        return response()->json($language_site);

//        return response()->json(['success' => true, 'data' => $model->all()]);
    }

    public function store(ContactUsStoreRequest $request)
    {
        $credentials = $request->all();
        $contactUs = ContactUS::create($credentials);


        Mail::send('mail.contactUs', compact('contactUs'), function ($message) use ($contactUs) {
            $message->to('info@scoutbeyond.com')
                ->subject('Contact Us');
        });

        return response()->json(['success' => true, 'data' => $contactUs]);
    }
}
