<?php

namespace App\Http\Controllers;

use App\HowItWork;
use App\Http\Requests\HowItWorkRequest;
use App\Key;
use Illuminate\Http\Request;
use App\Http\Traits\PhotoTrait;

class HowItWorkController extends Controller
{
    use PhotoTrait;

    public function index(int $language)
    {
        $language_site = HowItWork::where('language_id', $language)->get();
//        dd($language_site);
//        return response()->json($language_site);
        return response()->json($language_site);
    }

    public function showByKey()
    {

        $language_site = Key::whereHas('howItWork')->with('howItWork')->get();
//        $language_site = Menu::where('language_id', $language)->get();
//        return response()->json($language_site);
        return response()->json(['success' => $language_site]);
    }

    public function store(HowItWorkRequest $request)
    {
        $about = new HowItWork($request->all());
        $path = config('constants.image_folder.howItWork.save_path');
        $about->photo = $this->savePhoto($request->photo, $path);
        return response()->json(['success' => $about->save(), 'data' => $about]);
    }

    public function update(Request $request, HowItWork $howItWork)
    {
        $path = config('constants.image_folder.howItWork.save_path');
        if ($request->has('photo')) {
            if (!is_null($howItWork->photo)) {
                $this->deletePhoto($howItWork->getOriginal('photo'), $path);
            }
            $howItWork->photo = $this->savePhoto($request['photo'], $path);
        }
        return response()->json(['success' => $howItWork->update($request->except(['photo'])), 'data' => $howItWork]);

    }

    public function delete(HowItWork $howItWork)
    {
        return response()->json(['success' => $howItWork->delete(), 'message' => 'deleted']);
    }
}
