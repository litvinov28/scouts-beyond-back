<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Http\Requests\BannerRequest;
use App\Http\Requests\BannerUpdateRequest;
use App\Http\Traits\PhotoTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    use PhotoTrait;


    public function indexAll(Banner $banner)
    {

        return response()->json(['success' => $banner::all()]);
    }

//    public function index(Banner $banner)
    public function index(int $language)
    {
        $language_site = Banner::where('language_id', $language)->get();
        return response()->json($language_site);
//        return response()->json(['success' => $banner::all()]);
    }

    public function store(Banner $banner, BannerRequest $request)
    {
        $banner = new Banner($request->all());
        $path = config('constants.top_image_folder.top_image.save_path');
        $banner->photo =  $this->savePhoto($request->photo, $path);

        return response()->json(['success' => $banner->save(), 'data' => $banner]);
    }

    public function update(BannerUpdateRequest $request, Banner $banner)
    {


        $savePath =config('constants.top_image_folder.top_image.save_path');
        $isbase64 = Str::startsWith($request->photo, 'data');
        if ($isbase64) {
            if (!is_null($banner->photo)) {
                $this->deletePhoto($banner->photo, $savePath);
            }
            $banner->photo = $this->savePhoto($request['photo'], $savePath);
            return response()->json([
                'success' => $banner->update($request->except('photo')),
                'data' => $banner,
                'message' => 'Banner has been updated'
            ]);
        } else {
            return response()->json([
                'success' => $banner->update($request->all()),
                'data' => $banner,
                'message' => 'Banner has been updated'
            ]);
        }
    }
}
