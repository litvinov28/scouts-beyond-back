<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function store(CommentRequest $request)
    {
        $comment =  Comment::updateOrCreate([
            'report_id' => $request->input('report_id')
        ], $request->all());
        $comment->report()->update(['last_updated' => Carbon::parse($comment->updated_at)->format('Y-m-d H:i:s')]);
        return response()->json(['success' => $comment->save(), 'data' => $comment]);
    }

    public function update(Comment $comment, CommentRequest $request)
    {
        return response()->json(['success' => $comment->update($request->all()), 'data' => $comment]);
    }

    public function delete(Comment $comment)
    {
        return response()->json(['success' => $comment->delete(), 'message' => 'deleted']);
    }
}
