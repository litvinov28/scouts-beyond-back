<?php

namespace App\Http\Controllers;

use App\Http\Requests\OurTeamRequest;
use App\Http\Requests\TeamDeleteRequest;
use App\Http\Traits\PhotoTrait;
use App\OurTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class OurTeamController extends Controller
{
    use PhotoTrait;

    public function index(OurTeam $team)
    {
        return response()->json(['success' => $team::all()]);
    }

    public function store(OurTeam $team, OurTeamRequest $request)
    {
        $team = new OurTeam($request->all());
        $path = Config::get('constants.our_team_folder.our_teams.save_path');
        $gpath = Config::get('constants.our_team_folder.our_teams.get_path');
        $getPath = url($gpath);
        $team->photo = $getPath . '/' . $this->savePhoto($request->photo, $path);
        return response()->json(['success' => $team->save(), 'data' => $team]);


    }

    public function delete(OurTeam $team)
    {
        return response()->json(['success' => $team->delete(), 'message' => 'Deleted']);
    }
    public function destroy(TeamDeleteRequest $request){
        $credentials = $request->only('ids');
        OurTeam::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => 'deleted']);
    }

    public function update(Request $request, OurTeam $team)
    {
        $gpath = Config::get('constants.our_team_folder.our_teams.get_path');
        $getPath = url($gpath);
        if($request->has('photo')) {
            $savePath = Config::get('constants.our_team_folder.our_teams.save_path');
            if(!is_null($team->photo)){
                $this->deletePhoto($team->photo, $savePath);
            }
            $team->photo = $getPath.'/'. $this->savePhoto($request['photo'], $savePath);
        }
        return response()->json(['success' => $team->update($request->except(['photo'])), 'data' => $team, 'message' => 'OurTeam has been updated']);
    }
}
