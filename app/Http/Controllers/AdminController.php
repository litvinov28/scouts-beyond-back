<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Requests\AdminVideoDeleteRequest;
use App\Http\Requests\AdminVideoTextRequest;
use App\Http\Requests\AdminVideoUpdateRequest;
use App\Http\Resources\AdminVideoResource;
use App\Key;
use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\AdminVideo;
use App\Http\Requests\AdminVideoRequest;
use Illuminate\Support\Facades\Config;
use App\Http\Helpers\Video as VideoHelper;
use Illuminate\Support\Facades\DB;
use Vimeo\Laravel\Facades\Vimeo;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        $check = User::checkAdmin($credentials['email'], $credentials['password']);
        if (!$check) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.notExistAdmin')]);
        }
        $user = User::getByEmail($credentials['email']);
        $token = null;
        try {
            if (!$token = JWTAuth::fromUser($user)) {
                return response()->json(['success' => false, 'message' => trans('messages.errors.cantFindAccount')], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['success' => false, 'message' => trans('messages.errors.failedLogin')], 500);
        }
        return response()->json(['success' => $user, 'token' => $token]);
    }

    public function changePassword(Request $request, Admin $admin)
    {
        $credentials = $request->only('password', 'password_confirmation');
        $changedPassword = $admin->changePassword($credentials);
        return $changedPassword;
    }

    public function loadVideo(AdminVideoRequest $request, AdminVideo $video)
    {
        $currentUser = JWTAuth::user();

        $path = Config::get('constants.adminvideo_folder.adminvideo.save_path');
        $gpath = Config::get('constants.adminvideo_folder.adminvideo.get_path');
        $getPath = url($gpath);
        foreach ($request->section as $numOfSection => $videos) {
            foreach ($videos as $video) {
                $video = new VideoHelper($video, $path);
                $filename = $video->save();
            $vidos =    AdminVideo::create([
                    'section' => $numOfSection,
                    'path' => $getPath . '/' . $filename,
                    'user_id' => $currentUser->id,
                ]);
            }
        }
        return response()->json([
            'success' => true, 'message' => 'Video was successfully added',
            'data' => $vidos
        ]);
    }

    public function getVideo(AdminVideo $video)
    {
        return response()->json(['success' => $video::all()]);
    }

    public
    function update(AdminVideoUpdateRequest $request, AdminVideo $video)
    {
        $path = Config::get('constants.adminvideo_folder.adminvideo.save_path');
        $gpath = Config::get('constants.adminvideo_folder.adminvideo.get_path');
        $getPath = url($gpath);
        $video = AdminVideo::find($request->video_id);
        $newVideo = new VideoHelper($request->file, $path);
        $filename = $newVideo->save();
        $updatedVideo = $video->update([
            'path' => $getPath . '/' . $filename
        ]);
        return response()->json(['success' => $updatedVideo, 'data'=> $video]);
    }

    public
    function updateText(AdminVideoTextRequest $request)
    {
        $credentials = $request->only(['description', 'type']);
        $video = AdminVideo::find($request->id);
        $video->update(['description' => $credentials['description'], 'type' => $credentials['type']]);
        return response()->json(['success' => true, 'message' => trans('messages.success.updatedText'), 'data' => $video]);
    }

    public
    function delete()
    {
        DB::table('admin_videos')->truncate();
        return response()->json(['success' => true, 'message' => 'deleted']);
    }

    public
    function deleteId(AdminVideoDeleteRequest $request)
    {
        $credentials = $request->only('ids');
        AdminVideo::whereIn('id', $credentials['ids'])->delete();
        return response()->json(['success' => true, 'message' => trans('messages.success.deletedAdminVideo')]);
    }

    public function showByKey()
    {
        $keys = Key::whereHas('adminVideo')->with('adminVideo')->get();
        return response()->json(['success' => true,'data' => $keys]);
    }

    public function indexLanguage(int $language)
    {
        $language_site = AdminVideo::where('language_id', $language)->get();
        return response()->json(['success' => $language_site]);
    }
}
