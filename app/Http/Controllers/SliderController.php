<?php

namespace App\Http\Controllers;

use App\Http\Helpers\FormDataImage;
use App\Http\Requests\SliderRequest;
use App\Http\Requests\SliderUpdateRequest;
use App\Http\Traits\PhotoTrait;
use App\Key;
use App\Player;
use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Facades\JWTAuth;

class SliderController extends Controller
{
    use PhotoTrait;

    /*
     *
     * return list of products
     *
     * @return \Illuminate\Http\JsonResponse
     *
     */
    public function indexAll()
    {
        return response()->json(['success' => Slider::all()]);
    }
    public function index(int $language)
    {
        $language_site = Slider::where('language_id', $language)->get();
        return response()->json($language_site);
//        return response()->json(['success' => $banner::all()]);
    }

    public function showByKey()
    {
        $keys = Key::whereHas('slider')->with('slider')->get();
        return response()->json(['success' => true,'data' => $keys]);
    }

    /**
     * create a new Slider
     *Resource not found: NotFoundHttpException
     * @param Slider $slider
     * @param SliderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Slider $slider, SliderRequest $request)
    {
        $slider = new Slider($request->all());
        $path = config('constants.slider_folder.sliders.save_path');
        $slider->file =  $this->savePhoto($request->file, $path);
        Slider::query()->update([
            'time' => $slider->time,
        ]);
        return response()->json(['success' => $slider->save(), 'data' => $slider]);
    }

    public function delete(Slider $slider)
    {
        return response()->json(['success' => $slider->delete(), 'message' => 'Slider has been deleted']);
    }

    public function update(Request $request, Slider $slider)
    {
        if($request->has('file')) {
            $savePath = config('constants.slider_folder.sliders.save_path');
            if(!is_null($slider->file)){
                $this->deletePhoto($slider->file, $savePath);
            }
            $slider->file =  $this->savePhoto($request['file'], $savePath);
            Slider::query()->update([
                'time' => $slider->time,
            ]);
        }

        return response()->json(['success' => $slider->update($request->except(['file'])), 'data' => $slider, 'message' => 'Slider has been updated']);
    }

}
