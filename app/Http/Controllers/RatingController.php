<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Rating;
use Carbon;
Use Illuminate\Support\Facades\Mail;
use App\Mail as Email;
class RatingController extends Controller
{
    public function store(Rating $rating, RatingRequest $request)
    {

        $rating =  Rating::updateOrCreate([
            'report_id' => $request->input('report_id')
        ], $request->all());

        $rating->report()->update($request->only('status'));
//        $rating->result= substr(($request->rating1+$request->rating2+$request->rating3+$request->rating4)/4,'0','4');
        $rating->report()->update(['last_updated' =>  Carbon\Carbon::parse($rating->updated_at)->format('Y-m-d H:i:s')]);


        $email = $rating->report->player->user->email;
        $language = $rating->report->player->user->language_site_id;

        $mail = Email::where('language_id',$language)->where('key_id',44)->first();
        if ($rating->report->status == 2) {

            switch ($language){
                case 1:
                    Mail::send('mail.repot-prepared-coach', compact('mail'), function ($message) use ($email,$mail) {
                        $message->to($email)
                            ->subject($mail->subject);
                    });
                    break;
                case 2:
                    Mail::send('mail.repot-prepared-coach', compact('mail'), function ($message) use ($email,$mail) {
                        $message->to($email)
                            ->subject($mail->subject);
                    });

                    break;
            }


//            Mail::send('mail.report-admin-done', compact('rating', 'email'), function ($message) use ($email) {
//                $message->to('info@scoutbeyond.com')
//                    ->subject('report');
//            });
        }




        return response()->json(['success' => $rating->save(), 'data' => $rating]);
    }

    public function update(Rating $rating, RatingRequest $request) 
    {
        return response()->json(['success' => $rating->update($request->all()), 'data' => $rating]);
    }

    public function delete(Rating $rating)
    {
        return response()->json(['success' => $rating->delete(), 'message' => 'deleted']);
    }

}
