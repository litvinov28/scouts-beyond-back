<?php

namespace App\Http\Controllers;

use App\PlayerType;
use Illuminate\Http\Request;

class PlayerTypeController extends Controller
{
    public function index()
    {
        return response()->json(['success' => true,'data' => PlayerType::all()]);
    }
}
