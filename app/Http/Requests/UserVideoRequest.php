<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'section' => 'required|array|min:1|max:5',
            'section.1' => 'array|filled|min:1|max:1',
            'section.*' => 'array|filled|min:1|max:3',
          //  'section.*.*' => 'filled|file|mimetypes:video/x-ms-asf,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi,video/3gp'
        ];
    }
}
