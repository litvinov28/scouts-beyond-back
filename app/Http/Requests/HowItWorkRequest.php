<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HowItWorkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "title" => "required",
            "language_id" => "required",
            "key_id" => "required",
            "photo" => "required",
            "description" => "required"
        ];
    }
}
