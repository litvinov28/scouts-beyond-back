<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => '|max:55',
            'lastname' => '|max:55',
            'date_of_birth' => '|',
            'country_id' => '|max:55',
            'city_id' => '|max:55',
            'experience_id' => '|',
            'strong_leg' => '|',
            'field_position_id' => '|max:55',
            'preffered_position_id' => '|max:55',
            'language_id' => '|',
            'height' => '|',
            'weight' => '|',
            'gender_id' => '|',
            'is_active' => '|',
            'intrested_in_foreign' => '|',
            'has_agent' => '|',
            'has_contract' => '|',
            'contract_date' => '|',
            'about_me' => '|',
        ];
    }
}
