<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'report_id' => 'required|exists:reports,id',
            'parent_name' => 'required|',
            'parent_phone' => 'required|',
            'parent_email' => array('email', 'filled', 'regex:/^[+_.a-zA-Z0-9\'-]*\@+[_a-zA-Z0-9\'-\.]*\.+[_a-zA-Z0-9\'-\.]*$/'),
            'player_type' => 'filled|numeric',
            'state' => 'filled|in:draft,published',
            'section' => 'filled|array|min:1|max:5', 
     //       'section.*.*' => 'filled|file|mimetypes:video/x-ms-asf,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi,video/3gp'
        ];
    }
}
