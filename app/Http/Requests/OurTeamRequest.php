<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OurTeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|string|min:1',
            'photo'=> 'required|filled|',
            'country' => 'required|string|min:1',
            'description' => 'required|string|min:1',
            'section' => 'required|in:1,2',
        ];
    }
}
