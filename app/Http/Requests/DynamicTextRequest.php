<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DynamicTextRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required',
            'field1' => '|',
            'field2' => '|',
            'field3' => '|',
            'field4' => '|',
            'field5' => '|',
            'field6' => '|',
            'field7' => '|',
            'field8' => '|',
            'field9' => '|',
        ];
    }
}
