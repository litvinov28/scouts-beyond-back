<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminVideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'exists:users,id,role_id,3',
            'section' => 'required|array|min:1|max:5',
            'section.1' => 'array|filled|min:1|max:2',
            'section.*' => 'array|filled|min:1|max:4',
            'section.*.*' => 'filled'
        ];
    }
}
