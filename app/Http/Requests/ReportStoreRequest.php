<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'parent_name' => 'required|',
            'parent_phone' => 'required|alpha_num',
            'parent_email' => array('email', 'required', 'regex:/^[+_.a-zA-Z0-9\'-]*\@+[_a-zA-Z0-9\'-\.]*\.+[_a-zA-Z0-9\'-\.]*$/'),
            'player_type' => 'required|numeric',
//            'coach_id' => 'required|numeric|exists:users,id,role_id,2',
            'state' => 'required|in:draft,published',
            'section' => 'required|array|min:1|max:5', 
            'section.1' => 'array|filled|min:1|max:1',
            'section.*' => 'array|filled|min:1|max:3',
            'section.*.*' => 'filled|'
        ];
    }
}
