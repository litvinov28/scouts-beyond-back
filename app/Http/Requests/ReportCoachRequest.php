<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportCoachRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'coach_id' => 'required|numeric|exists:users,id,role_id,2',
            'status' => 'required|numeric|in:1,2,3',
            'id' => 'required|',
            'id.*' => 'numeric|distinct|exists:reports,id',

        ];
    }
}
