<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user() === null){
            return response()->json(['success' => false, 'message' => 'Insufficient permissions']);
        }
        $actions = $request->route()->getAction();
        $roles = isset($actions['roles']) ? $actions['roles'] : null;
        if(($request->user()->admin && in_array('Admin', $roles))){
            return $next($request);
        }
        return response()->json(['success' => false, 'message' => 'Insufficient permissions']);
    }
}
