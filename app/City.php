<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends BaseModel
{
    protected $fillable = ['name','country_id','latitude','longitude'];


//    public static function boot()
//    {
//
//        parent::boot();
//
//        static::creating(function ($table) {
//            $table->country_id = 1;
//
//        });
//
//    }

    public function player(){
        return $this->hasMany('App\Player');
    }

    public function country(){
        return $this->belongsTo('App\Country');
    }
}


