<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon;
class UserVideo extends BaseModel
{
    protected $fillable = [
        'user_id',
        'path',
        'section',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function getUrlAttribute() {
        $value = $this->path;
        if ($value) {
            return 'https://player.vimeo.com/video/' . $value;
        }
    }
}
