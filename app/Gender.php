<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends BaseModel
{
    protected $fillable = ['name', 'language_id','key_id'];

    public function player()
    {
        return $this->hasMany('App\Player');
    }

    public function keys(){
        return $this->belongsTo('App\Key','key_id');
    }
}
