<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends BaseModel
{
    protected $fillable = ['name', 'language_id','key_id'];

    public function contactus()
    {
        return $this->hasMany('App\ContactUs');
    }

    public function keys(){
        return $this->belongsTo('App\Key','key_id');
    }
}
