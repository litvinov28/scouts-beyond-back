<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends BaseModel
{
    protected $fillable = ['name', 'language_id','key_id'];

    public function player(){
        return $this->hasMany('App\PlayerLanguage');
    }

    public function keys(){
        return $this->belongsTo('App\Key', 'key_id');
    }
}
