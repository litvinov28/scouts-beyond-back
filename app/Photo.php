<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
    protected $fillable = [
      'name'
    ];

    public function getNameAttribute($value)
    {
        $getPath = config('constants.file_mail_photo.mail_photo.get_path');
        return url($getPath, $value);
    }
}
