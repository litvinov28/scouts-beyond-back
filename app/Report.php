<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends BaseModel
{
    protected $casts = ['last_updated'];
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'parent_name',
        'parent_phone',
        'parent_email',
        'player_type',
        'coach_id',
        'player_id',
        'state',
        'status',
        'visibility',
        'my_id',
        'due_date',
        'last_updated'
    ];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($table) {
            $table->status = 0;
            $table->visibility = 0;

        });
    }

    /*** RELATIONS ***/

    public function videos()
    {
        return $this->hasMany('App\Video');
    }

    public function player()
    {
        return $this->belongsTo('App\Player');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'coach_id');
    }

    public function rating()
    {
        return $this->hasMany('App\Rating');
    }

    public function comment()
    {
        return $this->hasOne('App\Comment');
    }

    public function invoice()
    {
        return $this->hasMany('App\Invoice');
    }
    /*** END-RELATIONS ***/
    public function scopeGetByUser($query, $user)
    {
        return $query->where('coach_id', $user->id);
    }
    public function scopeGetByPlayer($query, $user)
    {
        return $query->where('player_id', $user->id);
    }
//    public function getDueDateAttribute($timestamp) {
//        return Carbon\Carbon::parse($timestamp)->format( 'd.m.Y H:i:s');
//    }
//
//    public function getLastUpdatedAttribute($timestamp) {
//        return Carbon\Carbon::parse($timestamp)->format( 'd.m.Y H:i:s');
//    }
}
