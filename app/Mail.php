<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $fillable =
        [
          'subject',
          'text',
            'language_id',
            'key_id'
        ];

    public function keys(){
        return $this->belongsTo('App\Key','key_id');
    }

    public function language(){
        return $this->belongsTo('App\languageSite', 'language_id');
    }


}
