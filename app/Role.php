<?php

namespace App;

class Role extends BaseModel
{
    protected $fillable = ['name', 'description'];

    /*** RELATIONS ***/

    public function users(){
        return $this->hasMany('App\User', 'role_id', 'id');
    }

    /*** END-RELATIONS ***/
}
