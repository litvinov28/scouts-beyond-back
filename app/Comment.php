<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends BaseModel
{
    use SoftDeletes;
    protected $fillable = [

            'report_id',
            'comment1',
            'comment2',
            'comment3',
            'comment4',

    ];

    public function report()
    {
        return $this->belongsTo('App\Report');
    }
    public function scopeGetByReport($query, $report)
    {
        return $query->whereIn('report_id', $report);
    }
}
