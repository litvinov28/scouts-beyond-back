<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coach extends BaseModel
{
    protected $fillable = ['user_id'];
    
     /*** RELATIONS ***/

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*** END-RELATIONS ***/
}
