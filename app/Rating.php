<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends BaseModel
{
    use SoftDeletes;
    protected $fillable = [
        'report_id',
        'rating1',
        'rating2',
        'rating3',
        'rating4',
        'result'
    ];

    public static function boot()
    {

        parent::boot();

        static::creating(function ($table) {
            $table->result = ($table->rating1+$table->rating2+$table->rating3+$table->rating4)/4;
        });
    }

    public function report()
    {
        return $this->belongsTo('App\Report');
    }
    public function scopeGetByReport($query, $report)
    {
        return $query->whereIn('report_id', $report);
    }
}
