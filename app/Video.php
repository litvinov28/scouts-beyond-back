<?php

namespace App;

use Config;

class Video extends BaseModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path',
        'report_id',
        'section',
    ];
    
    /*** RELATIONS ***/
    
    public function report()
    {
        return $this->belongsTo('App\Report');
    }
    
    /*** END-RELATIONS ***/
    
    /** ACCESSORS **/
//
//    public function getUrlAttribute() {
//        $value = $this->path;
//        if ($value) {
//            return 'https://player.vimeo.com/video/' . $value;
//        }
//    }
    
    /** END ACCESSORS **/    
    
}
