<?php

namespace App;

class StaticText extends BaseModel
{
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'language_id'
    ];


    /*** SCOPES ***/

    public function scopeCheckById($query, $id){
        return $query->where('id', $id)->exists();
    }

    /*** END-SCOPES ***/



    public function languages(){
        return $this->belongsTo('App\languageSite', 'language_id');
    }

}
