<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileButton extends Model
{
    //

    protected $fillable = [
      'name',
      'key_id',
      'language_id'
    ];

    public function language(){
        return $this->belongsTo('App\languageSite', 'language_id');
    }

    public function keys(){
        return $this->belongsTo('App\Key','key_id');
    }

}
