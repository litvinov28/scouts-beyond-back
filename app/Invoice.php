<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class Invoice extends BaseModel
{
    public static function boot()
    {

        parent::boot();

        static::creating(function ($table) {
            $table->payment_id = 1;
            $table->user_id = Auth::user()->id;
        });
    }

    protected $fillable = ['user_id','report_id','payment_id','status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function report()
    {
        return $this->belongsTo('App\Report');
    }
    public function payment()
    {
        return $this->belongsTo('App\Payment');
    }
}
