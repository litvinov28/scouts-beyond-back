<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use phpDocumentor\Reflection\Types\Null_;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class User extends Authenticatable implements JWTSubject,MustVerifyEmail
{
    use Notifiable;
    use  SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'firstname',
        'lastname',
        'email',
        'password',
        'popup',
        'parent_phone',
        'verified',
        'language_site_id'
    ];

    protected $casts = [
        'popup' => 'boolean',
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /*** RELATIONS ***/

    public function admin()
    {
        return $this->hasOne('App\Admin');
    }

    public function player()
    {
        return $this->hasOne('App\Player');
    }

    public function uservideo()
    {
        return $this->hasMany('App\UserVideo');
    }

    public function adminvideo()
    {
        return $this->hasMany('App\AdminVideo');
    }

    public function report()
    {
        return $this->hasMany('App\Report');
    }

    public function invoice()
    {
        return $this->hasMany('App\Invoice');
    }

    public function coach()
    {
      return  $this->hasOne('App\Coach');
    }
    public function role(){
        return $this->belongsTo('App\Role');
    }
    public function languages(){
        return $this->hasMany('App\PlayerLanguage');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }

    /*** END-RELATIONS ***/

    /*** SCOPES ***/

    public function scopeCheckAdmin($query, $email, $password)
    {
        return $query->where('email', $email)->whereHas('admin', function ($q) use ($password) {
            $q->where('password', $password);
        })->exists();
    }


    public function scopeFilterRole($query, $roleName)
    {
        return $query->whereHas('role', function ($query) use ($roleName) {
            $query->whereIn('name', [$roleName,$roleName]);
        });
    }

    public function scopeGetByEmail($query, $email)
    {
        return $query->where('email', $email)->first();
    }

    /*** END-SCOPES ***/

    public function createUser($data)
    {
        $user = new User();
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];
        $user->language_site_id = $data['language_site_id'];
//        $user->verified = 1;
        if (isset($data['social_id'])) {
            $user->social_id = $data['social_id'];
            $user->social = $data['social'];
        } else {
            $user->password = Hash::make($data['password']);
        }
        if (isset($data['role'])) {
            $user->role_id = Role::where('name', $data['role'])->first()->id;
            $user->save();
        } else {
            $user->role_id = Role::where('name', 'Player')->first()->id;
            $user->save();
            $player = new Player;
            $player->user_id = $user->id;
            if (isset($data['social_id']))
                $player->avatar_url = $data['photoUrl'];
            $player->save();
            $language = new PlayerLanguage();
            $language->user_id = $user->id;
            $language->save();

        }
        return $user;
    }


    public function updateUser($data)
    {
        $player = Player::where('user_id', $this->id)->first();

        $this->firstname = $data['firstname'];
        $this->lastname = $data['lastname'];
        $this->popup = $data['popup'];
        $this->parent_phone = $data['parent_phone'];
        $this->save();
        $player->date_of_birth = $data['date_of_birth'] ?? NULL;
        $player->country_id = $data['country_id'];
        $player->city_id = $data['city_id'];
        $player->experience_id = $data['experience_id'];
        $player->strong_leg = $data['strong_leg'];
        $player->field_position_id = $data['field_position_id'];
        $player->preffered_position_id = $data['preffered_position_id'];
        $player->is_active = $data['is_active'];
        $player->height = $data['height'];
        $player->weight = $data['weight'];
        $player->gender_id = $data['gender_id'];
        $player->intrested_in_foreign = $data['intrested_in_foreign'];
        $player->has_agent = $data['has_agent'];
        $player->has_contract = $data['has_contract'];
        $player->contract_date = $data['contract_date'] ?? NULL;
        $player->about_me = $data['about_me'];
        $player->player_type_id = $data['player_type_id'];
        $player->soccer_team = $data['soccer_team'];
        $player->save();

        $this->languages()->delete();
        $langs = array_map(function ($id) {
            return new PlayerLanguage([
                'language_id' => $id,
            ]);
        }, $data['language_id']);

        $this->languages()->saveMany($langs);
        return $this;
    }


    public function createCoach($data)
    {
        $user = new User();
        $user->firstname = $data['firstname'];
        $user->lastname = $data['lastname'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->role_id = 2;
        $user->save();
        $coach = new Coach();
        $coach->user_id = $user->id;
        $coach->save();

        return $user;
    }



    /*** JWT ***/

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /*** END-JWT ***/

}
