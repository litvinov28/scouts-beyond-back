<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends BaseModel
{
    protected $fillable = ['title', 'subtitle', 'description', 'photo','language_id', 'key_id'];

    public function language(){
        return $this->belongsTo('App\LanguageSite','language_id');
    }


    public function getPhotoAttribute($value)
    {
        $getPath = config('constants.about_folder.about.get_path');
        return url($getPath . $value);
    }

    public function keys()
    {
        return $this->belongsTo('App\Key','key_id');
    }
}
