<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends BaseModel
{
    protected $fillable = ['title','description','file','time', 'language_id', 'key_id'];

    public function getFileAttribute($value)
    {
        $getPath = config('constants.slider_folder.sliders.get_path');
        return url($getPath . $value);
    }

    public function languages(){
        return $this->hasMany('App\Language','language_id');
    }
    public function keys(){
        return $this->belongsTo('App\Key', 'key_id');
    }
}
