<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends BaseModel
{
    protected $fillable = ['currency','total'];

    public function invoice(){
        return $this->hasMany('App\Invoice');
    }
}
