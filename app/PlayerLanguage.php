<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerLanguage extends Model
{
   protected $fillable = ['language_id', 'user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function language(){
        return $this->belongsTo('App\Language');
    }


    public function scopeByLanguage($query, $language)
    {
        if (!empty($language)) {
            return $query->whereIn('language_id', $language);
        }
        return $query;
    }
}
