<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon;
class OurTeam extends BaseModel
{
    protected $fillable = ['full_name','photo','description','country','section'];

    protected $dates = ['created_at' , 'updated_at'];

}
