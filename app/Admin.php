<?php

namespace App;

use Tymon\JWTAuth\Facades\JWTAuth;
use Carbon;
class Admin extends BaseModel
{
    protected $fillable = ['password', 'user_id'];

    protected $hidden = ['password'];

    /*** RELATIONS ***/

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /*** END-RELATIONS ***/

    public function changePassword($data){
        $currentUser = JWTAuth::parseToken()->authenticate();
        $admin = $currentUser->admin;
        if(!$admin){
            return response()->json(['success' => false, 'message' => trans('messages.errors.notAdmin')]);
        }
        $admin->update(['password' => $data['password']]);
        return response()->json(['success' => true, 'message' => trans('messages.success.successChangedPswd')]);
    }

}
