<?php

namespace App;


class Banner extends BaseModel
{
    protected $fillable = [
        'name',
        'title',
        'photo',
        'language_id'
    ];

    public function language(){
        return $this->belongsTo('App\languageSite', 'language_id');
    }

    public function getPhotoAttribute($value)
    {
        $getPath = config('constants.top_image_folder.top_image.get_path');
        return url($getPath . $value);
    }
}
