<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    
    const MESSAGE_VAR = 'data';
    
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            // detect previous instance
            if ($exception->getPrevious() instanceof TokenExpiredException) {
                return response()->json(['success' => 'false', 'message' => 'Token is expired.'], $exception->getStatusCode());
            }
            else if ($exception->getPrevious() instanceof TokenInvalidException) {
                return response()->json(['success' => 'false', 'message' => 'Token is invalid.'], $exception->getStatusCode());
            }
            else if ($exception->getPrevious() instanceof TokenBlacklistedException) {
                return response()->json(['success' => 'false', 'message' => 'Token is blacklisted'], $exception->getStatusCode());
            }
            else {
                return response()->json(['success' => 'false', 'message' => 'Token not provided'], $exception->getStatusCode());
            }
        }
        if ($request->is('api/*') || $request->wantsJson()) {
            $statusCode = method_exists($exception, 'getStatusCode') ? $exception->getStatusCode() : 500;
            $response = [
                'success' => false,
                self::MESSAGE_VAR => $exception->getMessage(),
            ];

            if ($exception instanceof ValidationException) {
                $response[self::MESSAGE_VAR] = collect($exception->errors())->flatten()->first();
                $statusCode = 422;
            }

            if ($exception instanceof UnauthorizedHttpException) {
                $response[self::MESSAGE_VAR] = 'Unauthorized';
                $statusCode = 401;
            }

            if ($exception instanceof ModelNotFoundException || $exception instanceof NotFoundHttpException) {
                $response[self::MESSAGE_VAR] = 'Resource not found: NotFoundHttpException';
                $statusCode = 404;
            }

            // This will replace our 405 response with a JSON response.
            if ($exception instanceof MethodNotAllowedHttpException) {
                $response[self::MESSAGE_VAR] = 'Method not allowed for this route: MethodNotAllowedException';
                $statusCode = 405;
            }


            // more info at localhost
            if (app('env') === 'local') {
                $response['file'] = $exception->getFile();
                $response['line'] = $exception->getLine();
                $response['trace'] = $exception->getTrace();
            }
            return response()->json($response, $statusCode);
        }
        return parent::render($request, $exception);
    }
}
