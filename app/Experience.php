<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends BaseModel
{
    protected $fillable = ['experience'];

    public function player(){
        return $this->belongsTo('App\Player');
    }
}
