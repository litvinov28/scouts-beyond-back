<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends BaseModel
{
    protected $fillable = ['name','url','location', 'language_id','key_id'];

    public function language(){
        return $this->belongsTo('App\LanguageSite','language_id');
    }

    public function keys(){
        return $this->belongsTo('App\Key','key_id');
    }

}
