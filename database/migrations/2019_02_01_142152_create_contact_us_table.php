<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_us', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname')->nullable(false);
            $table->integer('subject_id',false,10);
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->string('phone')->nullable(false);
            $table->string('email')->nullable(false);
            $table->string('message')->nullable(false);
            $table->integer('language_id', false, '10');
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('language_sites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_us');
    }
}
