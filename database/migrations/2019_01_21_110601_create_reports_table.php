<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('my_id',false,'10');
            $table->string('parent_name')->nullable();
            $table->string('parent_phone')->nullable();
            $table->string('parent_email')->nullable();

            $table->integer('player_type', false, 10);
            $table->integer('coach_id',false,'10')->nullable();
            $table->foreign('coach_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('player_id', false, 10);
            $table->foreign('player_id')->references('id')->on('players')->onDelete('cascade');
            $table->boolean('visibility');

            $table->timestamp('last_updated');
            $table->dateTime('due_date');
            $table->enum('state', ['draft', 'published']);
            $table->integer('status');

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['my_id', 'player_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
