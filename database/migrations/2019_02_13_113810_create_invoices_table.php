<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id',false,10);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('report_id',false,10);
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->integer('payment_id',false,10);
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
