<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id', false, 10);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->date('date_of_birth')->nullable();
            $table->integer('country_id',false,'10')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade');
            $table->integer('city_id',false,'10')->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
            $table->string('avatar_src')->nullable();
            $table->string('avatar_url')->nullable();
            $table->integer('experience_id',false,10)->nullable();
            $table->foreign('experience_id')->references('id')->on('experiences')->onDelete('cascade');

            $table->integer('player_type_id',false,10)->nullable();
            $table->foreign('player_type_id')->references('id')->on('player_types')->onDelete('cascade');

            $table->string('soccer_team')->nullable();
            $table->string('resume')->nullable();

            $table->enum('strong_leg', ['שמאל', 'ימין'])->nullable();

            $table->integer('field_position_id',false,'10')->nullable();
            $table->foreign('field_position_id')->references('id')->on('positions')->onDelete('cascade');
            $table->integer('preffered_position_id',false,'10')->nullable();
            $table->foreign('preffered_position_id')->references('id')->on('positions')->onDelete('cascade');
            $table->float('height', 10)->nullable();
            $table->float('weight', 10)->nullable();
            $table->integer('gender_id',false,'10')->nullable();
            $table->foreign('gender_id')->references('id')->on('genders')->onDelete('cascade');
            $table->boolean('is_active')->nullable();
            $table->boolean('intrested_in_foreign')->nullable();
            $table->boolean('has_agent')->nullable();
            $table->boolean('has_contract')->nullable();
            $table->date('contract_date')->nullable();
            $table->text('about_me')->nullable();

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
