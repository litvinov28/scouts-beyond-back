<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileButtonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_buttons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('language_id', false,   '10')->nullable();
            $table->integer('key_id', false,   '10')->nullable();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('language_sites');
            $table->foreign('key_id')->references('id')->on('keys');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_buttons');
    }
}
