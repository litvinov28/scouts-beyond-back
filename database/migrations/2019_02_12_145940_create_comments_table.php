<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id',false,10)->unique();
            $table->foreign('report_id')->references('id')->on('reports')->onDelete('cascade');
            $table->string('comment1',1000)->nullable();
            $table->string('comment2',1000)->nullable();
            $table->string('comment3',1000)->nullable();
            $table->string('comment4',1000)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
