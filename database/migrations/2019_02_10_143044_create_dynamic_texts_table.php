<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDynamicTextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dynamic_texts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('field1');
            $table->text('field2');
            $table->text('field3');
            $table->text('field4');
            $table->text('field5');
            $table->text('field6');
            $table->text('field7');
            $table->text('field8');
            $table->text('field9');
            $table->integer('language_id',false,'10')->nullable();
            $table->timestamps();

            $table->foreign('language_id')->references('id')->on('language_sites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dynamic_texts');
    }
}
