<?php

use Illuminate\Database\Seeder;

class KeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('keys')->insert([
            'id' => '1',
            'name' => 'user_info',
        ]);

        DB::table('keys')->insert([
            'id' => '2',
            'name' => 'user_video',
        ]);

        DB::table('keys')->insert([
            'id' => '3',
            'name' => 'video_report',
        ]);

        DB::table('keys')->insert([
            'id' => '4',
            'name' => 'show_report',
        ]);
        DB::table('keys')->insert([
            'id' => '5',
            'name' => 'mail_user',
        ]);
        DB::table('keys')->insert([
            'id' => '6',
            'name' => 'facebook',
        ]);
        DB::table('keys')->insert([
            'id' => '7',
            'name' => 'settings',
        ]);
        DB::table('keys')->insert([
            'id' => '8',
            'name' => 'gender_male',
        ]);
        DB::table('keys')->insert([
            'id' => '9',
            'name' => 'gender_female',
        ]);
        DB::table('keys')->insert([
            'id' => '10',
            'name' => 'technical_support_subjects',
        ]);
        DB::table('keys')->insert([
            'id' => '11',
            'name' => 'feedback_subjects',
        ]);
        DB::table('keys')->insert([
            'id' => '12',
            'name' => 'common_subjects',
        ]);
        DB::table('keys')->insert([
            'id' => '13',
            'name' => 'registration_how_it_work',
        ]);
        DB::table('keys')->insert([
            'id' => '14',
            'name' => 'upload_videos_report',
        ]);
        DB::table('keys')->insert([
            'id' => '15',
            'name' => 'receiving_report_back',
        ]);
        DB::table('keys')->insert([
            'id' => '16',
            'name' => 'find_best_players',
        ]);
        DB::table('keys')->insert([
            'id' => '17',
            'name' => 'invite_to_a_tournament',
        ]);
        DB::table('keys')->insert([
            'id' => '18',
            'name' => 'home_page_menu_top',
        ]);
        DB::table('keys')->insert([
            'id' => '19',
            'name' => 'search_players_menu_top',
        ]);
        DB::table('keys')->insert([
            'id' => '20',
            'name' => 'about_us_menu_down',
        ]);
        DB::table('keys')->insert([
            'id' => '21',
            'name' => 'our_team_menu_down',
        ]);
        DB::table('keys')->insert([
            'id' => '22',
            'name' => 'about_us_menu_top',
        ]);
        DB::table('keys')->insert([
            'id' => '23',
            'name' => 'our_team_menu_top',
        ]);
        DB::table('keys')->insert([
            'id' => '24',
            'name' => 'our_team_menu_top',
        ]);
        DB::table('keys')->insert([
            'id' => '25',
            'name' => 'hebrew_language',
        ]);
        DB::table('keys')->insert([
            'id' => '26',
            'name' => 'english_language',
        ]);
        DB::table('keys')->insert([
            'id' => '27',
            'name' => 'contact-us_menus_top',
        ]);
        DB::table('keys')->insert([
            'id' => '28',
            'name' => 'contact-us_menus_down',
        ]);
        DB::table('keys')->insert([
            'id' => '29',
            'name' => 'how-it-work_menus_top',
        ]);
        DB::table('keys')->insert([
            'id' => '30',
            'name' => 'how-it-work_menus_down',
        ]);
        DB::table('keys')->insert([
            'id' => '31',
            'name' => 'privacy_menus_down',
        ]);
        DB::table('keys')->insert([
            'id' => '32',
            'name' => 'terms-of-use_menus_down',
        ]);
        DB::table('keys')->insert([
            'id' => '33',
            'name' => 'slider-1',
        ]);
        DB::table('keys')->insert([
            'id' => '34',
            'name' => 'slider-2',
        ]);
        DB::table('keys')->insert([
            'id' => '35',
            'name' => 'slider-3',
        ]);
        DB::table('keys')->insert([
            'id' => '36',
            'name' => 'about_us_1',
        ]);
        DB::table('keys')->insert([
            'id' => '37',
            'name' => 'about_us_2',
        ]);
        DB::table('keys')->insert([
            'id' => '38',
            'name' => 'about_us_3',
        ]);

        DB::table('keys')->insert([
            'id' => '39',
            'name' => 'mail_register',
        ]);

        DB::table('keys')->insert([
            'id' => '40',
            'name' => 'mail_deleted_account',
        ]);
        DB::table('keys')->insert([
            'id' => '41',
            'name' => 'mail_trainer_report_sent_successfully',
        ]);
        DB::table('keys')->insert([
            'id' => '42',
            'name' => 'mail_temp_password',
        ]);
        DB::table('keys')->insert([
            'id' => '43',
            'name' => 'mail_admin_send_password',
        ]);
        DB::table('keys')->insert([
            'id' => '44',
            'name' => 'mail_report_coach_prepared',
        ]);
        DB::table('keys')->insert([
            'id' => '45',
            'name' => 'profile_coach_popup_1',
        ]);
        DB::table('keys')->insert([
            'id' => '46',
            'name' => 'profile_coach_popup_2',
        ]);
        DB::table('keys')->insert([
            'id' => '47',
            'name' => 'profile_coach_popup_3',
        ]);
        DB::table('keys')->insert([
            'id' => '48',
            'name' => 'profile_coach_popup_4',
        ]);
        DB::table('keys')->insert([
            'id' => '49',
            'name' => 'profile_coach_popup_5',
        ]);
        DB::table('keys')->insert([
            'id' => '50',
            'name' => 'profile_coach_popup_6',
        ]);
        DB::table('keys')->insert([
            'id' => '51',
            'name' => 'freestyle',
        ]);
        DB::table('keys')->insert([
            'id' => '52',
            'name' => 'practice_videos',
        ]);
        DB::table('keys')->insert([
            'id' => '53',
            'name' => 'goalkeeper_field',
        ]);
        DB::table('keys')->insert([
            'id' => '54',
            'name' => 'brake_field',
        ]);
        DB::table('keys')->insert([
            'id' => '55',
            'name' => 'right_shield_field',
        ]);
        DB::table('keys')->insert([
            'id' => '56',
            'name' => 'left_shield_field',
        ]);
        DB::table('keys')->insert([
            'id' => '57',
            'name' => 'central_connection_field',
        ]);
        DB::table('keys')->insert([
            'id' => '58',
            'name' => 'right_connection_field',
        ]);
        DB::table('keys')->insert([
            'id' => '59',
            'name' => 'left_link_field',
        ]);
        DB::table('keys')->insert([
            'id' => '60',
            'name' => 'center_forward_field',
        ]);
        DB::table('keys')->insert([
            'id' => '61',
            'name' => 'right_pioneer_field',
        ]);
        DB::table('keys')->insert([
            'id' => '62',
            'name' => 'left_pioneer_field',
        ]);
        DB::table('keys')->insert([
            'id' => '63',
            'name' => 'goalkeeper_preffered',
        ]);
        DB::table('keys')->insert([
            'id' => '64',
            'name' => 'brake_shield_preffered',
        ]);
        DB::table('keys')->insert([
            'id' => '65',
            'name' => 'link_preffered',
        ]);
        DB::table('keys')->insert([
            'id' => '66',
            'name' => 'pioneer_preffered',
        ]);
        DB::table('keys')->insert([
            'id' => '67',
            'name' => 'alert_empty_fields',
        ]);
        DB::table('keys')->insert([
            'id' => '68',
            'name' => 'alert_empty_firstname',
        ]);
        DB::table('keys')->insert([
            'id' => '69',
            'name' => 'alert_empty_lastname',
        ]);
        DB::table('keys')->insert([
            'id' => '70',
            'name' => 'alert_empty_repeat_password',
        ]);
        DB::table('keys')->insert([
            'id' => '71',
            'name' => 'alert_empty_password',
        ]);
        DB::table('keys')->insert([
            'id' => '72',
            'name' => 'alert_empty_email',
        ]);
        DB::table('keys')->insert([
            'id' => '73',
            'name' => 'alert_error_email',
        ]);
        DB::table('keys')->insert([
            'id' => '74',
            'name' => 'alert_error_password',
        ]);
        DB::table('keys')->insert([
            'id' => '75',
            'name' => 'alert_not_confirm',
        ]);
        DB::table('keys')->insert([
            'id' => '76',
            'name' => 'alert_success_password_sended',
        ]);
        DB::table('keys')->insert([
            'id' => '77',
            'name' => 'alert_error_password_sended',
        ]);
        DB::table('keys')->insert([
            'id' => '78',
            'name' => 'alert_incorrect_data',
        ]);
        DB::table('keys')->insert([
            'id' => '79',
            'name' => 'alert_password_not_exist',
        ]);
        DB::table('keys')->insert([
            'id' => '80',
            'name' => 'alert_empty_contract_date',
        ]);
        DB::table('keys')->insert([
            'id' => '81',
            'name' => 'alert_empty_about',
        ]);
        DB::table('keys')->insert([
            'id' => '82',
            'name' => 'alert_empty_height',
        ]);
        DB::table('keys')->insert([
            'id' => '83',
            'name' => 'alert_empty_weight',
        ]);
        DB::table('keys')->insert([
            'id' => '84',
            'name' => 'alert_video_deleted',
        ]);
        DB::table('keys')->insert([
            'id' => '85',
            'name' => 'alert_video_uploaded',
        ]);
        DB::table('keys')->insert([
            'id' => '86',
            'name' => 'alert_not_scout',
        ]);
        DB::table('keys')->insert([
            'id' => '87',
            'name' => 'alert_phone_saved',
        ]);
        DB::table('keys')->insert([
            'id' => '88',
            'name' => 'alert_error_phone',
        ]);
        DB::table('keys')->insert([
            'id' => '89',
            'name' => 'alert_user_data_saved',
        ]);
        DB::table('keys')->insert([
            'id' => '90',
            'name' => 'alert_not_register',
        ]);
        DB::table('keys')->insert([
            'id' => '91',
            'name' => 'alert_not_login',
        ]);
        DB::table('keys')->insert([
            'id' => '92',
            'name' => 'alert_not_pdf',
        ]);
        DB::table('keys')->insert([
            'id' => '93',
            'name' => 'alert_error_name',
        ]);
        DB::table('keys')->insert([
            'id' => '94',
            'name' => 'alert_error_user_type',
        ]);
        DB::table('keys')->insert([
            'id' => '95',
            'name' => 'alert_error_terms_of_use',
        ]);
        DB::table('keys')->insert([
            'id' => '96',
            'name' => 'alert_error_section_size',
        ]);
        DB::table('keys')->insert([
            'id' => '97',
            'name' => 'alert_error_format',
        ]);
        DB::table('keys')->insert([
            'id' => '98',
            'name' => 'alert_empty_section',
        ]);
        DB::table('keys')->insert([
            'id' => '99',
            'name' => 'alert_success_created',
        ]);
        DB::table('keys')->insert([
            'id' => '100',
            'name' => 'alert_success_edited',
        ]);
        DB::table('keys')->insert([
            'id' => '101',
            'name' => 'alert_success_saved',
        ]);
        DB::table('keys')->insert([
            'id' => '102',
            'name' => 'alert_wait_upload',
        ]);
        DB::table('keys')->insert([
            'id' => '103',
            'name' => 'alert_empty_message',
        ]);
        DB::table('keys')->insert([
            'id' => '104',
            'name' => 'alert_success_send',
        ]);
        DB::table('keys')->insert([
            'id' => '105',
            'name' => 'alert_select_topic',
        ]);
        DB::table('keys')->insert([
            'id' => '106',
            'name' => 'alert_user_exist',
        ]);
        DB::table('keys')->insert([
            'id' => '107',
            'name' => 'alert_empty_phone',
        ]);
        DB::table('keys')->insert([
            'id' => '108',
            'name' => 'alert_empty_fullname',
        ]);
        DB::table('keys')->insert([
            'id' => '109',
            'name' => 'alert_error_video_size',
        ]);
        DB::table('keys')->insert([
            'id' => '110',
            'name' => 'alert_error_try_later',
        ]);
        DB::table('keys')->insert([
            'id' => '111',
            'name' => 'section_one_null_null',
        ]);
        DB::table('keys')->insert([
            'id' => '112',
            'name' => 'self_presentation_section_one',
        ]);
        DB::table('keys')->insert([
            'id' => '113',
            'name' => 'self_presentation_section_two',
        ]);
        DB::table('keys')->insert([
            'id' => '114',
            'name' => 'handling_personal_ball',
        ]);
        DB::table('keys')->insert([
            'id' => '115',
            'name' => 'photo_tutorial_video__section_one',
        ]);
        DB::table('keys')->insert([
            'id' => '116',
            'name' => 'training_video_dribbling',
        ]);
        DB::table('keys')->insert([
            'id' => '117',
            'name' => 'photo_tutorial_video_section_three',
        ]);
        DB::table('keys')->insert([
            'id' => '118',
            'name' => 'kicks_gate',
        ]);
        DB::table('keys')->insert([
            'id' => '119',
            'name' => 'photo_tutorial_video_section_four',
        ]);
        DB::table('keys')->insert([
            'id' => '120',
            'name' => 'video_games',
        ]);
        DB::table('keys')->insert([
            'id' => '121',
            'name' => 'photo_tutorial_video_section_five',
        ]);
        DB::table('keys')->insert([
            'id' => '122',
            'name' => 'hand_ball_handling_technique',
        ]);
        DB::table('keys')->insert([
            'id' => '123',
            'name' => 'photo_tutorial_video_section_two',
        ]);
        DB::table('keys')->insert([
            'id' => '124',
            'name' => 'technique_with_foot',
        ]);
        DB::table('keys')->insert([
            'id' => '125',
            'name' => 'photo_tutorial_video_section_three',
        ]);
        DB::table('keys')->insert([
            'id' => '126',
            'name' => 'leaps_ball',
        ]);
        DB::table('keys')->insert([
            'id' => '127',
            'name' => 'photo_tutorial_video_section_four',
        ]);
        DB::table('keys')->insert([
            'id' => '128',
            'name' => '1-on-1_player',
        ]);
        DB::table('keys')->insert([
            'id' => '129',
            'name' => 'photo_tutorial_video_section_five',
        ]);



































    }
}
