<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***TECHNICAL-SUPPORT***/
        DB::table('subjects')->insert([
            'name' => 'technical support subjects',
            'language_id' => '1',
            'key_id' => '10',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'תמיכה טכנית',
            'language_id' => '2',
            'key_id' => '10',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***FEEDBACK***/
        DB::table('subjects')->insert([
            'name' => 'feedback subjects',
            'language_id' => '1',
            'key_id' => '11',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'פידבק',
            'language_id' => '2',
            'key_id' => '11',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***COMMON***/
        DB::table('subjects')->insert([
            'name' => 'common subjects',
            'language_id' => '1',
            'key_id' => '12',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'כללי',
            'language_id' => '2',
            'key_id' => '12',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);












    }
}
