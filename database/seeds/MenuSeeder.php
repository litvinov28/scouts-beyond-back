<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***HOME***/
        DB::table('menus')->insert([
            'name' => 'home page',
            'url' => 'home',
            'location' => 'top',
            'language_id' => '1',
            'key_id' => '18',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'דף הבית',
            'url' => 'home',
            'location' => 'top',
            'language_id' => '2',
            'key_id' => '18',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***SEARCH-PLAYERS***/
        DB::table('menus')->insert([
            'name' => 'Search Players',
            'url' => 'searchPlayers',
            'location' => 'top',
            'language_id' => '1',
            'key_id' => '19',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'חיפוש שחקנים',
            'url' => 'searchPlayers',
            'location' => 'top',
            'language_id' => '2',
            'key_id' => '19',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        /***OUR-TEAM***/
        DB::table('menus')->insert([
            'name' => 'Our Team',
            'url' => 'ourTeam',
            'location' => 'down',
            'language_id' => '1',
            'key_id' => '21',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'אודות הצוות',
            'url' => 'ourTeam',
            'location' => 'down',
            'language_id' => '2',
            'key_id' => '21',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***ABOUT-US-DOWN***/
        DB::table('menus')->insert([
            'name' => 'About Us',
            'url' => 'aboutUs',
            'location' => 'top',
            'language_id' => '1',
            'key_id' => '22',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'עלינו',
            'url' => 'aboutUs',
            'location' => 'top',
            'language_id' => '2',
            'key_id' => '22',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
//        /***OUR-TEAM-DOWN***/
//        DB::table('menus')->insert([
//            'name' => 'Our Team',
//            'url' => 'ourTeam',
//            'location' => 'top',
//            'language_id' => '1',
//            'key_id' => '23',
//            'created_at' => \Carbon\Carbon::now(),
//            'updated_at' => \Carbon\Carbon::now()
//        ]);
//        DB::table('menus')->insert([
//            'name' => 'אודות הצוות',
//            'url' => 'ourTeam',
//            'location' => 'top',
//            'language_id' => '2',
//            'key_id' => '23',
//            'created_at' => \Carbon\Carbon::now(),
//            'updated_at' => \Carbon\Carbon::now()
//        ]);

        /***HOW-IT-WORK-TOP***/
        DB::table('menus')->insert([
            'name' => 'How it work',
            'url' => 'how-it-work',
            'location' => 'top',
            'language_id' => '1',
            'key_id' => '29',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'איך זה עובד',
            'url' => 'how-it-work',
            'location' => 'top',
            'language_id' => '2',
            'key_id' => '29',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        /***CONTACT-US-TOP***/
        DB::table('menus')->insert([
            'name' => 'Contact Us',
            'url' => 'contact-us',
            'location' => 'top',
            'language_id' => '1',
            'key_id' => '27',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'צור קשר',
            'url' => 'contact-us',
            'location' => 'top',
            'language_id' => '2',
            'key_id' => '27',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);




        /***PRIVACY***/
        DB::table('menus')->insert([
            'name' => 'Privacy',
            'url' => 'privacy',
            'location' => 'down',
            'language_id' => '1',
            'key_id' => '31',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'מדיניות הפרטיות',
            'url' => 'privacy',
            'location' => 'down',
            'language_id' => '2',
            'key_id' => '31',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***TERMS-OF-USE***/
        DB::table('menus')->insert([
            'name' => 'Terms of use',
            'url' => 'terms-of-use',
            'location' => 'down',
            'language_id' => '1',
            'key_id' => '32',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'תנאי השימוש',
            'url' => 'terms-of-use',
            'location' => 'down',
            'language_id' => '2',
            'key_id' => '32',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***ABOUT-US-DOWN***/
        DB::table('menus')->insert([
            'name' => 'About Us',
            'url' => 'aboutUs',
            'location' => 'down',
            'language_id' => '1',
            'key_id' => '20',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'עלינו',
            'url' => 'aboutUs',
            'location' => 'down',
            'language_id' => '2',
            'key_id' => '20',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***CONTACT-US-DOWN***/
        DB::table('menus')->insert([
            'name' => 'Contact Us',
            'url' => 'contact-us',
            'location' => 'down',
            'language_id' => '1',
            'key_id' => '28',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'צור קשר',
            'url' => 'contact-us',
            'location' => 'down',
            'language_id' => '2',
            'key_id' => '28',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***HOW-IT-WORK-DOWN***/
        DB::table('menus')->insert([
            'name' => 'How it work',
            'url' => 'how-it-work',
            'location' => 'down',
            'language_id' => '1',
            'key_id' => '30',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('menus')->insert([
            'name' => 'איך זה עובד',
            'url' => 'how-it-work',
            'location' => 'down',
            'language_id' => '2',
            'key_id' => '30',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
