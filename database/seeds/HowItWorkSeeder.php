<?php

use Illuminate\Database\Seeder;

class HowItWorkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /***REGISTRATION***/
        DB::table('how_it_works')->insert([
            'title' => 'Registration',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326154709.png',
            'language_id' => '1',
            'key_id' => '13',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('how_it_works')->insert([
            'title' => 'הרשמה',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326154709.png',
            'language_id' => '2',
            'key_id' => '13',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***UPLOAD-VIDEO-REPORT***/
        DB::table('how_it_works')->insert([
            'title' => 'Upload videos to a report',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326936124.png',
            'language_id' => '1',
            'key_id' => '14',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('how_it_works')->insert([
            'title' => 'העלה סרטונים לדוח',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326936124.png',
            'language_id' => '2',
            'key_id' => '14',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***RECEIVING-REPORT-BACK***/
        DB::table('how_it_works')->insert([
            'title' => 'Receiving report back',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326936124.png',
            'language_id' => '1',
            'key_id' => '15',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('how_it_works')->insert([
            'title' => 'קבלת דוח בחזרה',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326936124.png',
            'language_id' => '2',
            'key_id' => '15',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***FIND-BEST-PLAYERS***/
        DB::table('how_it_works')->insert([
            'title' => 'Find best players',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326310564.png',
            'language_id' => '1',
            'key_id' => '16',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('how_it_works')->insert([
            'title' => 'מצא את השחקנים הטובים ביותר',
            'subtitle' => NULL,
            'description' => 'Lorem ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_1571326310564.png',
            'language_id' => '2',
            'key_id' => '16',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***INVITE-TOURNAMENT***/
        DB::table('how_it_works')->insert([
            'title' => 'Invite to a tournament',
            'subtitle' => NULL,
            'description' => 'Lorem2 ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_157132635061.png',
            'language_id' => '1',
            'key_id' => '17',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('how_it_works')->insert([
            'title' => 'הזמן לטורניר',
            'subtitle' => NULL,
            'description' => 'Lorem2 ipsum dolor sit amet,' .
                ' consectetur adipiscing elit, sed do eiusmod ' .
                'tempor incididunt ut labore et dolore' .
                ' magna aliqua. Quis ipsum suspendisse' .
                ' ultrices gravida. Risus commodo viverra ' .
                'maecenas accumsan lacus vel facilisis.',
            'photo' => 'image_157132635061.png',
            'language_id' => '2',
            'key_id' => '17',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

    }
}
