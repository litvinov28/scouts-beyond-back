<?php

use Illuminate\Database\Seeder;
use App\City;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $city = new City();
        $city->country_id = 2;
        $city->name = 'Aba';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Abakaliki';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Abeokuta';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Abonnema';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Abuja';
        $city->save();


        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ado Ekiti';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Afikpo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Agbor';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Agulu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Aku';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Akure';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Amaigbo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ankpa';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Asaba';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Auchi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Awka';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Azare';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Bama';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Bauchi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Bende';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Benin City';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Bida';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Birnin Kebbi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Biu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Buguma';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Calabar';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Damaturu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Daura';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Dutse';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ede';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Effium';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Effon Alaiye';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Eha Amufu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ejigbo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ekpoma';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Enugu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Enugu Ukwu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Epe';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Etiti';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ezza Inyimagu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Funtua';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Gamboru';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Gashua';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Gboko';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Gbongan';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Gombe';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Gusau';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Hadejia';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ibadan';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Idah';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ife';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ifo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ifon';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Igboho';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Igbo Ora';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Igbo Ukwu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ihiala';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ijebu Igbo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ijebu Ode';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ijero';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikare';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikeja';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikerre';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikire';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikirun';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikom';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikorodu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ikot Ekpene';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ila Orangun';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ilawe Ekiti';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ilesha';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ilobu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ilorin';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Inisa';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ise';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Iseyin';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ishieke';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Iwo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Jalingo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Jimeta';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Jos';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kaduna';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kafanchan';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kagoro';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kano';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Katsina';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kaura Namoda';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Keffi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kishi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kontagora';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Kuroko';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Lafia';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Lagos';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Lokoja';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Maiduguri';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Makurdi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Malumfashi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Minna';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Modakeke';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Mubi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Nguru';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Nkpor';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Nnewi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Nsukka';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Numan';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Obosi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Offa';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ogaminan';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ogbomosho';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ohafia';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Oka Akoko';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Okene';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Okigwi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Okitipupa';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Okpogho';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Okrika';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ondo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Onitsha';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Oron';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Oshogbo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Otukpo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Owerri';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Owo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Oyo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ozubulu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Port Harcourt';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Sagamu';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Sango Otta';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Sapele';
        $city->save();


        $city = new City();
        $city->country_id = 2;
        $city->name = 'Shaki';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Sokoto';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Suleja';
        $city->save();


        $city = new City();
        $city->country_id = 2;
        $city->name = 'Uga';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ugep';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Ughelli';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Umuahia';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Uromi';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Uyo';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Warri';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Wukari';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Yenagoa';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Yola';
        $city->save();

        $city = new City();
        $city->country_id = 2;
        $city->name = 'Zaria';
        $city->save();
    }
}
