<?php

use Illuminate\Database\Seeder;

class ProfileButtonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***USER-INFO***/
        DB::table('profile_buttons')->insert([
            'name' => 'user_info',
            'key_id' => '1',
            'language_id' => '1',
        ]);

        DB::table('profile_buttons')->insert([
            'name' => 'פרטי משתמש',
            'key_id' => '1',
            'language_id' => '2',
        ]);

        /***USER-VIDEO***/
        DB::table('profile_buttons')->insert([
            'name' => 'user_video',
            'key_id' => '2',
            'language_id' => '1',
        ]);
        DB::table('profile_buttons')->insert([
            'name' => 'סרטוני פריסטייל/סרטוני משחק ותרגול',
            'key_id' => '2',
            'language_id' => '2',
        ]);

        /***VIDEO-REPORT***/
        DB::table('profile_buttons')->insert([
            'name' => 'video_report',
            'key_id' => '3',
            'language_id' => '1',
        ]);
        DB::table('profile_buttons')->insert([
            'name' => 'בקשה לדו"ח מאמן חדש',
            'key_id' => '3',
            'language_id' => '2',
        ]);

        /***SHOW-REPORT***/
        DB::table('profile_buttons')->insert([
            'name' => 'show_report',
            'key_id' => '4',
            'language_id' => '1',
        ]);
        DB::table('profile_buttons')->insert([
            'name' => 'צפייה בדוחות מאמן',
            'key_id' => '4',
            'language_id' => '2',
        ]);

        /***MAIL-USER***/
        DB::table('profile_buttons')->insert([
            'name' => 'mail_user',
            'key_id' => '5',
            'language_id' => '1',
        ]);
        DB::table('profile_buttons')->insert([
            'name' => 'הודעות',
            'key_id' => '5',
            'language_id' => '2',
        ]);

        /***FACEBOOK***/
        DB::table('profile_buttons')->insert([
            'name' => 'facebook',
            'key_id' => '6',
            'language_id' => '1',
        ]);
        DB::table('profile_buttons')->insert([
            'name' => 'שיתוף הפרופיל בפייסבוק',
            'key_id' => '6',
            'language_id' => '2',
        ]);

        /***SETTING***/
        DB::table('profile_buttons')->insert([
            'name' => 'settings',
            'key_id' => '7',
            'language_id' => '1',
        ]);
        DB::table('profile_buttons')->insert([
            'name' => 'הגדרות',
            'key_id' => '7',
            'language_id' => '2',
        ]);
    }
}
