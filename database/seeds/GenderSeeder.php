<?php

use Illuminate\Database\Seeder;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***GENDER-MALE***/
        DB::table('genders')->insert([
            'name' => 'gender_male',
            'language_id' => '1',
            'key_id' => '8',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('genders')->insert([
            'name' => 'גבר',
            'language_id' => '2',
            'key_id' => '8',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        /***GENDER-FEMALE***/
        DB::table('genders')->insert([
            'name' => 'gender_female',
            'language_id' => '1',
            'key_id' => '9',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('genders')->insert([
            'name' => 'נקבה',
            'language_id' => '2',
            'key_id' => '9',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
