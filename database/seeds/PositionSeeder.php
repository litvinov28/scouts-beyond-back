<?php

use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('positions')->insert([
            'position' => 'goalkeeper',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '53',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'שוער',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '53',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);


        DB::table('positions')->insert([
            'position' => 'brake',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '54',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'בלם',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '54',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Right shield',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '55',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'מגן ימני',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '55',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Left Shield',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '56',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'מגן שמאלי',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '56',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Central connection',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '57',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'קשר מרכזי',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '57',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Right connection',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '58',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'קשר ימני',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '58',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'left link',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '59',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'קשר שמאלי',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '59',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Center forward',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '60',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'חלוץ מרכזי',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '60',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Right pioneer',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '61',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'חלוץ ימני',
            'type' => 'field',
            'language_id' => '2',
            'key_id' => '61',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Left pioneer',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '62',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'חלוץ שמאלי',
            'type' => 'field',
            'language_id' => '1',
            'key_id' => '62',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'goalkeeper',
            'type' => 'preffered',
            'language_id' => '1',
            'key_id' => '63',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'שוער',
            'type' => 'preffered',
            'language_id' => '2',
            'key_id' => '63',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'Brake / Shield',
            'type' => 'preffered',
            'language_id' => '1',
            'key_id' => '64',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'בלם/מגן',
            'type' => 'preffered',
            'language_id' => '2',
            'key_id' => '64',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'link',
            'type' => 'preffered',
            'language_id' => '1',
            'key_id' => '65',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'קשר',
            'type' => 'preffered',
            'language_id' => '2',
            'key_id' => '65',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('positions')->insert([
            'position' => 'pioneer',
            'type' => 'preffered',
            'language_id' => '1',
            'key_id' => '66',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('positions')->insert([
            'position' => 'חלוץ',
            'type' => 'preffered',
            'language_id' => '2',
            'key_id' => '66',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }


}
