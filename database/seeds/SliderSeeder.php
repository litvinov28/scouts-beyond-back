<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->insert([
            'title' => 'Want to be the next star in soccer and play in the worlds top clubs?',
            'description' => 'Scout Beyond, the leading online platform for developing, promoting and discovering young football talents, accompanies you throughout this process!',
            'file' => 'https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/sliders/Artboard_1.png',
            'time' => '7000',
            'language_id' => '1',
            'key_id' => '33',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('sliders')->insert([
            'title' => 'הפלטפורמה המובילה לפיתוח, קידום וגילוי של כשרונות כדורגל צעירים',
            'description' => 'ScoutBeyond היא רשת סקאוטינג מקצועיתהמיועדת לשחקני כדורגל צעירים ולקבוצות כדורגל המחפשות את הכוכב הבא בתחום.',
            'file' => 'https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/sliders/Artboard_1.png',
            'time' => '7000',
            'language_id' => '2',
            'key_id' => '33',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /******/
        DB::table('sliders')->insert([
            'title' => 'Want to get reviews from experienced coaches and advance your playing career?',
            'description' =>  'Scout Beyond, the leading online platform for developing, promoting and discovering young football talents, accompanies you throughout this process!',
            'file' => 'https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/sliders/Artboard_2.png',
            'time' => '7000',
            'language_id' => '1',
            'key_id' => '34',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('sliders')->insert([
            'title' => 'בואו ותראו לכל העולם כמה אתם באמת מוכשרים!',
            'description' => 'הפלטפורמה הייחודית שלנו מאפשרת לכל שחקן כדורגל צעיר להקים לעצמו פרופיל ולקבל דוחות מאמן מאחד מאנשי הצוות הבכירים שלנו.',
            'file' => 'https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/sliders/Artboard_2.png',
            'time' => '7000',
            'language_id' => '2',
            'key_id' => '34',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        /******/
        DB::table('sliders')->insert([
            'title' => 'Something',
            'description' => 'Something here',
            'file' => 'https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/sliders/image_1571327857690.png',
            'time' => '7000',
            'language_id' => '1',
            'key_id' => '35',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('sliders')->insert([
            'title' => 'Something',
            'description' => 'Something here',
            'file' => 'https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/sliders/image_1571327857690.png',
            'time' => '7000',
            'language_id' => '2',
            'key_id' => '35',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
