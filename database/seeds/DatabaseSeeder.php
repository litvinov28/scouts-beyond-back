<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(AdminSeeder::class);
        $this->call(KeySeeder::class);
        $this->call(languageSiteSeeder::class);
        $this->call(StaticTextSeeder::class);
        $this->call(ExperienceSeeder::class);
        $this->call(PositionSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(LanguageSeeder::class);
        $this->call(GenderSeeder::class);
        $this->call(SubjectSeeder::class);
        $this->call(DymanicTextSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(MenuSeeder::class);
        $this->call(PaymentSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(PlayerTypeSeeder::class);
        $this->call(ProfileButtonSeeder::class);
        $this->call(HowItWorkSeeder::class);
        $this->call(BannerSeeder::class);
        $this->call(AboutUsSeeder::class);
        $this->call(MailSeeder::class);
        $this->call(AdminVideoSeeder::class);
    }
}
