<?php

use Illuminate\Database\Seeder;
use App\PlayerType;
class PlayerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $player = new PlayerType();
        $player->name = 'test';
        $player->save();
    }
}
