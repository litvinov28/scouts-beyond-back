<?php

use App\Admin;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->email = 'info@scoutbeyond.com';
        $user->role_id = 3;
        $user->password = Hash::make('Scout123$');;
        $user->save();
        $admin = new Admin();
        $user->admin()->save($admin);
    }
}
