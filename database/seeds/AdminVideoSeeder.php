<?php

use Illuminate\Database\Seeder;

class AdminVideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '1',
            'type' => NULL,
            'description' => NULL,
            'path' => 'https://player.vimeo.com/video/337098538',
            'language_id' => '1',
            'key_id' => '111',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '1',
            'type' => NULL,
            'description' => NULL,
            'path' => 'https://player.vimeo.com/video/337098538',
            'language_id' => '2',
            'key_id' => '111',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);



        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '1',
            'type' => '1',
            'description' => 'Tutorial Video: Self Presentation',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1559143130_RAlaGwfv.mp4',
            'language_id' => '1',
            'key_id' => '112',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '1',
            'type' => '1',
            'description' => 'סרטון הדרכה: הצגה עצמית',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1559143130_RAlaGwfv.mp4',
            'language_id' => '2',
            'key_id' => '112',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '1',
            'type' => '2',
            'description' => 'Tutorial Video: Self Presentation',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1559143653_AqOSOHTt.mp4',
            'language_id' => '1',
            'key_id' => '113',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '1',
            'type' => '2',
            'description' => 'סרטון הדרכה: הצגה עצמית',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1559143653_AqOSOHTt.mp4',
            'language_id' => '2',
            'key_id' => '113',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '2',
            'description' => 'Tutorial video: Handling a personal ball',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558467333_9cJ09547.mp4',
            'language_id' => '1',
            'key_id' => '114',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '2',
            'description' => 'סרטון הדרכה: טיפול בכדור אישי',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558467333_9cJ09547.mp4',
            'language_id' => '2',
            'key_id' => '114',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '2',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558467552_PnVuM0Wh.mp4',
            'language_id' => '1',
            'key_id' => '115',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '2',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558467552_PnVuM0Wh.mp4',
            'language_id' => '2',
            'key_id' => '115',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '2',
            'description' => 'Training video: Dribbling',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558468345_KI7yDGfj.mp4',
            'language_id' => '1',
            'key_id' => '116',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '2',
            'description' => 'סרטון הדרכה: כדרור',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558468345_KI7yDGfj.mp4',
            'language_id' => '2',
            'key_id' => '116',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '2',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558468540_jbuQbcVm.mp4',
            'language_id' => '1',
            'key_id' => '117',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '2',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558468540_jbuQbcVm.mp4',
            'language_id' => '2',
            'key_id' => '117',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '2',
            'description' => 'Tutorial video: Kicks to the gate',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558468821_04AtaKUQ.mp4',
            'language_id' => '1',
            'key_id' => '118',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '2',
            'description' => 'סרטון הדרכה: בעיטות לשער',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558468821_04AtaKUQ.mp4',
            'language_id' => '2',
            'key_id' => '118',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '2',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558469078_XI6SKEbH.mp4',
            'language_id' => '1',
            'key_id' => '119',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '2',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558469078_XI6SKEbH.mp4',
            'language_id' => '2',
            'key_id' => '119',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '2',
            'description' => 'Tutorial Video: Games',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558469643_jYkw0xgc.mp4',
            'language_id' => '1',
            'key_id' => '120',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '2',
            'description' => 'סרטון הדרכה: משחקונים',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558469643_jYkw0xgc.mp4',
            'language_id' => '2',
            'key_id' => '120',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '2',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558469834_0bBj0aFN.mp4',
            'language_id' => '1',
            'key_id' => '121',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '2',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558469834_0bBj0aFN.mp4',
            'language_id' => '2',
            'key_id' => '121',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '1',
            'description' => 'Tutorial Video: Hand Ball Handling Technique',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558470286_yKCHGyHt.mp4',
            'language_id' => '1',
            'key_id' => '122',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '1',
            'description' => 'סרטון הדרכה: טכניקת טיפול בכדור עם היד',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558470286_yKCHGyHt.mp4',
            'language_id' => '2',
            'key_id' => '122',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '1',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558470518_nrGyVJeC.mp4',
            'language_id' => '1',
            'key_id' => '123',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '2',
            'type' => '1',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558470518_nrGyVJeC.mp4',
            'language_id' => '2',
            'key_id' => '123',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '1',
            'description' => 'Tutorial video: A ball therapy technique with the foot',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558470981_mKxWWoPF.mp4',
            'language_id' => '1',
            'key_id' => '124',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '1',
            'description' => 'סרטון הדרכה: טכניקת טיפול בכדור עם הרגל',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558470981_mKxWWoPF.mp4',
            'language_id' => '2',
            'key_id' => '124',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '1',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558471230_b2lW7FrI.mp4',
            'language_id' => '1',
            'key_id' => '125',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '3',
            'type' => '1',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558471230_b2lW7FrI.mp4',
            'language_id' => '2',
            'key_id' => '125',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '1',
            'description' => 'Training video: leaps to the ball',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558471527_CA4vR2Xa.mp4',
            'language_id' => '1',
            'key_id' => '126',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '1',
            'description' => 'סרטון הדרכה: זינוקים לכדור',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558471527_CA4vR2Xa.mp4',
            'language_id' => '2',
            'key_id' => '126',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '1',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558471800_YVSOqQIx.mp4',
            'language_id' => '1',
            'key_id' => '127',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '4',
            'type' => '1',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558471800_YVSOqQIx.mp4',
            'language_id' => '2',
            'key_id' => '127',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '1',
            'description' => 'Tutorial video: 1-on-1 of a player in front of a goalkeeper',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558472090_XESQYaFB.mp4',
            'language_id' => '1',
            'key_id' => '128',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '1',
            'description' => 'סרטון הדרכה: 1 על 1 של שחקן מול שוער',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558472090_XESQYaFB.mp4',
            'language_id' => '2',
            'key_id' => '128',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '1',
            'description' => 'Photo tutorial video',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558472294_cesK528N.mp4',
            'language_id' => '1',
            'key_id' => '129',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('admin_videos')->insert([
            'user_id' => '330',
            'section' => '5',
            'type' => '1',
            'description' => 'סרטון הדרכת צילום',
            'path' => 'https://scoutbeyond.com/api/storage/video/admin/file_1558472294_cesK528N.mp4',
            'language_id' => '2',
            'key_id' => '129',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }







































}
