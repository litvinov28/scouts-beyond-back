<?php

use Illuminate\Database\Seeder;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('banners')->insert([
            'title' => 'Scout Beyond',
            'name' => 'description',
            'photo' => 'image_1552409707169.png',
            'language_id' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('banners')->insert([
            'title' => 'Scout Beyond',
            'name' => 'description',
            'photo' => 'image_1552409707169.png',
            'language_id' => '2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('banners')->insert([
            'title' => 'Request for coach report',
            'name' => 'Reports',
            'photo' => 'image_1552409707169.png',
            'language_id' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('banners')->insert([
            'title' => 'בקשה לדוח מאמן',
            'name' => 'Reports',
            'photo' => 'image_1552409707169.png',
            'language_id' => '2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('banners')->insert([
            'title' => 'Scout Beyond Team',
            'name' => 'OurTeam',
            'photo' => 'image_1551974711240.png',
            'language_id' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('banners')->insert([
            'title' => 'צוות Scout Beyond',
            'name' => 'OurTeam',
            'photo' => 'image_1551974711240.png',
            'language_id' => '2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('banners')->insert([
            'title' => ' Terms of Use',
            'name' => 'Privacy',
            'photo' => 'image_1554800395939.png',
            'language_id' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('banners')->insert([
            'title' => 'תנאי השימוש באתר',
            'name' => 'Privacy',
            'photo' => 'image_1554800395939.png',
            'language_id' => '2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);


        DB::table('banners')->insert([
            'title' => 'Privacy Policy',
            'name' => 'Privacy',
            'photo' => 'image_1554800847757.png',
            'language_id' => '1',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('banners')->insert([
            'title' => 'מדיניות הפרטיות',
            'name' => 'Privacy',
            'photo' => 'image_1554800847757.png',
            'language_id' => '2',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);


    }
}
