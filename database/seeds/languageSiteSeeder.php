<?php

use Illuminate\Database\Seeder;

class languageSiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('language_sites')->insert([
            'name' => 'English',
        ]);
        DB::table('language_sites')->insert([
            'name' => 'עברית',
        ]);
    }
}
