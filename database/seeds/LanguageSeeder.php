<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***Hebrew***/
        DB::table('languages')->insert([
            'name' => 'Hebrew',
            'language_id' => '1',
            'key_id' => '25',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('languages')->insert([
            'name' => 'עברית',
            'language_id' => '2',
            'key_id' => '25',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***English***/
        DB::table('languages')->insert([
            'name' => 'English',
            'language_id' => '1',
            'key_id' => '26',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('languages')->insert([
            'name' => 'אנגלית',
            'language_id' => '2',
            'key_id' => '26',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);









    }
}
