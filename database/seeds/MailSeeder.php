<?php

use Illuminate\Database\Seeder;

class MailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mails')->insert([
            'subject' => 'ברוכים הבאים!',
            'language_id' => 2,
            'key_id' => 39,

            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0" dir="rtl"
       style="background: url(https://scoutbeyond.com/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: \'Assistant\', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img" src="https://scoutbeyond.com/api/storage/img/photos/header-bg2.png" width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: center;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scoutbeyond.com/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                <span style="background: #ffffff;">ברוכים הבאים לאתר</span> <img src="https://scoutbeyond.com/api/storage/img/photos/logo.png" alt="logo" style="width: 96px;padding-right: 10px;vertical-align: bottom;"></h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>

        <td style="text-align: center; width:66%;">
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">  היי  $firstname, <br></p>

            <p style="padding: 10px 0px; text-align: right;">      ברוך הבא לאתר Scout Beyond<br>
                ברוך הבא לפלטפורמה האיכותית שלנו!
            </p>
            <p style="text-align: right;">כדי להיכנס בפעמים הבאות לפרופיל המשתמש שלך, תצטרך להשתמש בכתובת המייל ובסיסמה אותן סיפקת לאתר בזמן ההרשמה. לכן, דאג לרשום אותן לעצמך, או לשמור אותן בדרך אחרת. </p>
            <p style="text-align: right;padding:10px 0px">
                לאחר הכניסה המזוהה לאתר, תוכל להתחיל וליהנות מהיתרונות שהאתר מעניק לך.         </p>

            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">שחקן?</p>
            <p style="text-align: right;">

                דאג למלא את כל הפרטים בפרופיל השחקן שלך, וכמובן - אל תשכח להגיש בקשה לקבלת דו"ח מאמן מאחד מהמאמנים המובילים שבצוות Scout Beyond .
                חשוב לזכור כי פרופילים של שחקנים שכבר קיבלו דו"ח מאמן אחד לפחות, נוטים לבלוט יותר לעיניהם של מועדונים וסוכנים.

            </p>
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">קבוצה/סקאוט/סוכן?</p>
            <p style="text-align: right;">בדף חיפוש השחקנים ישנה רשימה מלאה של השחקנים שבאתר, עם ציונים ליד כל פרופיל. תוכל לסנן את השחקנים הרלוונטיים ביותר בשבילך, ואף לאתר שחקנים לפי פרמטרים ספציפיים דרך מנוע החיפוש שבאתר.

            </p>
            <p style="text-align: right;"> <br>    צוות Scout Beyond מאחל לך בהצלחה,
                <br>                ועומד לרשותך בכל בעיה ב-info@scoutbeyond.com

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <a style="color: white; background-image:url(https://scoutbeyond.com/api/storage/img/photos/button-bg.png);background-size: 100% 100%;text-decoration: none; padding-top: 17px;padding-bottom: 17px;padding-right: 49px;padding-left: 49px;" href="https://scoutbeyond.com/home">    לחץ כאן למעבר לאתר </a>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> <span
                        style="font-weight: 600;">כל הזכויות שמורות ל</span> scoutbeyond.com</p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span> info@scoutbeyond.com</p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>'
        ]);
        DB::table('mails')->insert([
            'subject' => 'Welcome!',
            'language_id' => 1,
            'key_id' => 39,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<p>&nbsp;</p>

 
 <p>          Hello, $firstname!</p>

        
<p>&nbsp;</p>
</body>
</html>'
        ]);
        DB::table('mails')->insert([
            'subject' => 'אישור מחיקת הפרופיל באתר',
            'language_id' => 2,
            'key_id' => 40,

            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0" dir="rtl"
       style="background: url(https://scoutbeyond.com/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: \'Assistant\', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img" src="https://scoutbeyond.com/api/storage/img/photos/userDelete1.png" width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: right;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scoutbeyond.com/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                <img src="https://scoutbeyond.com/api/storage/img/photos/logo.png" alt="logo" style="width: 96px;padding-right: 10px;vertical-align: bottom;"><span style="background: #ffffff;">מחיקת פרופיל באתר</span></h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>
        <td style="text-align: center; width:66%;">
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">, $firstname  שלום </p>
            <p style="padding: 10px 0 0; text-align: right;margin: 0;">מחיקת הפרופיל בוצעה בהצלחה.
                על מנת שתוכל לחזור ולהשתמש באותו פרופיל יהיה עליך לפנות לתמיכה
            </p>
            <p style="margin: 0;text-align: right;"><a href="mailto:support@scoutbeyond.com">info@scoutbeyond.com</a> שלנו בכתובת   </p>
            <p style="margin: 0;text-align: right;">    .לצורך איפוס החשבון שלך</p> <p style="margin: 0;text-align: right;">  <br>  בברכה,
            </p>
            <p style="margin: 0;text-align: right;">  <br>  Scout Beyond  <span>צוות</span>
            </p>


            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> scoutbeyond.com <span
                        style="font-weight: 600;">כל הזכויות שמורות ל </span></p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">info@scoutbeyond.com
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span></p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>'
        ]); DB::table('mails')->insert([
            'subject' => 'Confirm deletion of the site profile',
            'language_id' => 1,
            'key_id' => 40,

            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<p>&nbsp;</p>

<p>Hello, $firstname !</p>
<p>Successfully deleted profile.</p>
<p>To be able to use the same profile again, you must contact support</p>

   
<p>&nbsp;</p>
</body>
</html>'
    ]);

        DB::table('mails')->insert([
            'subject' => 'דו"ח מאמן חדש נכנס למערכת',
            'language_id' => 2,
            'key_id' => 41,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0"
       style="background: url(https://scoutbeyond.com/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: \'Assistant\', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img" src="https://scoutbeyond.com/api/storage/img/photos/header-bg5.png" width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: right;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scoutbeyond.com/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                    <span style="background: #ffffff;">   דו"ח מספר $value נשלח בהצלחה </span>
            </h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>

        <td style="text-align: center; width:66%;">
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">,שלום מאמן יקר
            </p>

            <p style="padding: 10px 0px; text-align: right;"> דו״ח מאמן חדש נכנס למערכת. עליך להגיב על הדו״ח תוך 7 ימים. כדי לצפות בפרטי הדו״ח עליך להיכנס לפרופיל שלך לאזור ״צפייה בדוחות מאמן״ לאחר כתיבת הדו״ח יהיה עליך ללחוץ על כפתור השליחה שבתחתית הדף</p>
            <p style="text-align: right;margin: 0;">,תודה</p>
            <p style="text-align: right;margin-top: 0;">Scouts beyond צוות</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> scoutbeyond.com <span
                        style="font-weight: 600;">כל הזכויות שמורות ל </span></p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">info@scoutbeyond.com
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span></p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>'

     ]);      DB::table('mails')->insert([
            'subject' => 'A new coach report has entered the system',
            'language_id' => 1,
            'key_id' => 41,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<p>&nbsp;</p>

                
                    <span style="background: #ffffff;"> Report number $value sent successfully </span>
                
            <p>A new coach report came into the system. You must respond to the report within 7 days. To view the report details, you must log in to your profile in the "View Coach Reports" area after writing the report, you must click the submit button at the bottom of the page </p>
        
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
       
<p>&nbsp;</p>
</body>
</html>'
        ]);
  DB::table('mails')->insert([
            'subject' =>  'קבלת סיסמא זמנית',
            'language_id' => 2,
            'key_id' => 42,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0"
       style="background: url(https://scoutbeyond.com/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: \'Assistant\', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img" src="https://scoutbeyond.com/api/storage/img/photos/header_bg4.1.png" width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: right;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scoutbeyond.com/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                <span style="background: #ffffff;">הסיסמא הזמנית שלך</span></h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>

        <td style="text-align: center; width:66%;">
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;"> ,$firstname שלום </p>

            <p style="padding: 10px 0px; text-align: right;">הנפקנו עבורך סיסמא זמנית אותה תצטרך לשנות תוך 24 שעות</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <span style="color: black; background-image:url(https://scoutbeyond.com/api/storage/img/photos/button-bg1.png);background-size: 100% 100%;text-decoration: none; padding-top: 17px;padding-bottom: 17px;padding-right: 49px;padding-left: 49px;">$pass</span>
            </p>
            <p>&nbsp;</p>
            <p style="text-align: right;">על מנת להחליף סיסמא זו עליך להיכנס לאזור ההגדרות שבפרופיל שלך ולשנות את סיסמא תחת הכותרת שינוי סיסמא</p>
            <p style="text-align: right;">אנו מאחלים לך המשך גלישה נעימה באתר</p>
            <p style="text-align: right;">Sсoutbeyond צוות</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <a style="color: white; background-image:url(https://scoutbeyond.com/api/storage/img/photos/button-bg.png);background-size: 100% 100%;text-decoration: none; padding-top: 17px;padding-bottom: 17px;padding-right: 49px;padding-left: 49px;" href="https://scoutbeyond.com/home">    לחץ כאן למעבר לאתר </a>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> scoutbeyond.com <span
                        style="font-weight: 600;">כל הזכויות שמורות ל </span></p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">info@scoutbeyond.com
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span></p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>'

     ]);
  DB::table('mails')->insert([
            'subject' => 'new password',
            'language_id' => 1,
            'key_id' => 42,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<p>&nbsp;</p>


            <p> Hello, $firstname </p>

            <p>We issued you a temporary password which you will need to change within 24 hours</p>
            <p>
          <p> $pass</p>
            </p>
            <p>&nbsp;</p>
       
 

<p>&nbsp;</p>
</body>
</html>'

     ]);
     DB::table('mails')->insert([
            'subject' => 'קבלת סיסמא זמנית',
            'language_id' => 2,
            'key_id' => 43,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0"
       style="background: url(https://scoutbeyond.com/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: \'Assistant\', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img" src="https://scoutbeyond.com/api/storage/img/photos/header_bg4.1.png" width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: right;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scoutbeyond.com/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                <span style="background: #ffffff;">הסיסמא הזמנית שלך</span></h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>

        <td style="text-align: center; width:66%;">
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;"> ,$name שלום </p>

            <p style="padding: 10px 0px; text-align: right;">הנפקנו עבורך סיסמא זמנית אותה תצטרך לשנות תוך 24 שעות</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <span style="color: black; background-image:url(https://scoutbeyond.com/api/storage/img/photos/button-bg1.png);background-size: 100% 100%;text-decoration: none; padding-top: 17px;padding-bottom: 17px;padding-right: 49px;padding-left: 49px;">$pass</span>
            </p>
            <p>&nbsp;</p>
            <p style="text-align: right;">על מנת להחליף סיסמא זו עליך להיכנס לאזור ההגדרות שבפרופיל שלך ולשנות את סיסמא תחת הכותרת שינוי סיסמא</p>
            <p style="text-align: right;">אנו מאחלים לך המשך גלישה נעימה באתר</p>
            <p style="text-align: right;">Sсoutbeyond צוות</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <a style="color: white; background-image:url(https://scoutbeyond.com/api/storage/img/photos/button-bg.png);background-size: 100% 100%;text-decoration: none; padding-top: 17px;padding-bottom: 17px;padding-right: 49px;padding-left: 49px;" href="https://scoutbeyond.com/home">    לחץ כאן למעבר לאתר </a>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> scoutbeyond.com <span
                        style="font-weight: 600;">כל הזכויות שמורות ל </span></p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">info@scoutbeyond.com
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span></p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>
'

     ]);  DB::table('mails')->insert([
            'subject' => 'new password',
            'language_id' => 1,
            'key_id' => 43,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<p>&nbsp;</p>


            <p> Hello, $name </p>

            <p>We issued you a temporary password which you will need to change within 24 hours</p>
            <p>
          <p> $pass</p>
            </p>
            <p>&nbsp;</p>
       
 

<p>&nbsp;</p>
</body>
</html>'

     ]);
     DB::table('mails')->insert([
            'subject' => 'קבלת סיסמא זמנית',
            'language_id' => 2,
            'key_id' => 44,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0"
       style="background: url(https://scoutbeyond.com/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: \'Assistant\', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img"
                             src="https://scoutbeyond.com/api/storage/img/photos/header-bg5(edit).png"
                             width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: center;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scoutbeyond.com/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                <span style="background: #ffffff;">דו"ח המאמן שלך מוכן לצפייה</span></h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>
        <td style="text-align: center; width:66%;">

            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;"> </p>


            <p style="padding: 10px 0px; text-align: right;"> הדו"ח מאמן שהגשת נבדק על ידי אחד מאנשי הצוות המקצועי שלנו
                והוא מוכן כעת לצפייה</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <a style="color: white; background-image:url(https://scoutbeyond.com/api/storage/img/photos/button-bg.png);background-size: 100% 100%;text-decoration: none; padding-top: 17px;padding-bottom: 17px;padding-right: 49px;padding-left: 49px;"
                   href="https://scoutbeyond.com/home/">לחץ כאן לצפייה בדו"ח מאמן </a>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> scoutbeyond.com <span
                        style="font-weight: 600;">כל הזכויות שמורות ל </span></p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">info@scoutbeyond.com
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span></p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>
'

     ]);

     DB::table('mails')->insert([
            'subject' => 'קבלת סיסמא זמנית',
            'language_id' => 1,
            'key_id' => 44,
            'text' => '<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>

<p>&nbsp;</p>

                <span>Your coach report is ready to watch</span>
       
<p>&nbsp;</p>
</body>
</html>
'

     ]);
    }
}
