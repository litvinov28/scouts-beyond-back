<?php

use Illuminate\Database\Seeder;

class AboutUsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        /***Scout-Beyond***/
        DB::table('abouts')->insert([
            'title' => 'Scout Beyond',
            'subtitle' => 'Who are we and what is our purpose?',
            'description' => 'Scout Beyond is a professional platform that appeals to all young footballers at the outset, to teams, agents and talent scouts. We chose to focus our platform on young Israeli soccer players, because we think the future lies with them, with those kids and teens who see their future in the field of football and before setting up the platform, haven\'t been given the opportunity to reveal their talent.
',
            'photo' => 'image_1550582595617.png',
            'language_id' => '1',
            'key_id' => '36',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('abouts')->insert([
            'title' => 'Scout Beyond',
            'subtitle' => 'מי אנחנו ומהי מטרתנו?',
            'description' => 'Scout Beyond היא פלטפורמה מקצועית שפונה לכל שחקני הכדורגל הצעירים בראשית דרכם, לקבוצות, לסוכנים ולמגלי הכישרונות. בחרנו למקד את הפלטפורמה שלנו דווקא בשחקני כדורגל ישראלים צעירים, מאחר ואנחנו חושבים שהעתיד נמצא אצלם, אצל אותם ילדים ובני נוער שרואים את עתידם בתחום הכדורגל ולפני הקמת הפלטפורמה, לא קיבלו את ההזדמנות המתאימה לחשוף את הכישרון שלהם.',
            'photo' => 'image_1550582595617.png',
            'language_id' => '2',
            'key_id' => '36',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***Scout-Beyond***/
        DB::table('abouts')->insert([
            'title' => 'contact',
            'subtitle' => 'Who are we and what is our purpose?',
            'description' => 'Scout Beyond is a professional platform that appeals to all young footballers at the outset, to teams, agents and talent scouts. We chose to focus our platform on young Israeli soccer players, because we think the future lies with them, with those kids and teens who see their future in the field of football and before setting up the platform, haven\'t been given the opportunity to reveal their talent.
',
            'photo' => 'image_1550582855657.png',
            'language_id' => '1',
            'key_id' => '37',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('abouts')->insert([
            'title' => 'צרו קשר',
            'subtitle' => 'אנחנו כאן בשבילכם',
            'description' => 'מערכת סקאוטינג לקבוצות עם אפשרויות סינון מתקדם של שחקנים. הפלטפורמה מאפשרת לקבוצות, סוכנים וסקאוטים לקבל כמה שיותר מידע על השחקן, וזאת, מבלי להיאלץ לצאת ולבחון אותו בשטח. בעזרת אומדנים ותרגילים מקצועיים המוצגים בדו"ח מאמן אשר נבדק על ידי אחד מאנשי הצוות המקצועי הבכיר שלנו, יכולות הקבוצות לקבל מידע מקיף על השחקן ועל היכולות שלו.',
            'photo' => 'image_1550582855657.png',
            'language_id' => '2',
            'key_id' => '37',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        /***Scout-Beyond***/
        DB::table('abouts')->insert([
            'title' => 'Scout Beyond',
            'subtitle' => 'System',
            'description' => 'Scouting system for teams with advanced player filtering options. The platform allows teams, agents and scouts to get as much information as possible about the player, without having to go out and examine him on the ground. With the help of professional estimates and exercises presented in a coach report reviewed by one of our senior professional staff, teams can obtain comprehensive information about the player and their abilities.',
            'photo' => 'image_1550582996589.png',
            'language_id' => '1',
            'key_id' => '38',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
        DB::table('abouts')->insert([
            'title' => 'Scout Beyond',
            'subtitle' => 'המערכת',
            'description' => 'מערכת סקאוטינג לקבוצות עם אפשרויות סינון מתקדם של שחקנים. הפלטפורמה מאפשרת לקבוצות, סוכנים וסקאוטים לקבל כמה שיותר מידע על השחקן, וזאת, מבלי להיאלץ לצאת ולבחון אותו בשטח. בעזרת אומדנים ותרגילים מקצועיים המוצגים בדו"ח מאמן אשר נבדק על ידי אחד מאנשי הצוות המקצועי הבכיר שלנו, יכולות הקבוצות לקבל מידע מקיף על השחקן ועל היכולות שלו.',
            'photo' => 'image_1550582996589.png',
            'language_id' => '2',
            'key_id' => '38',
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
//        /***Scout-Beyond***/
//        DB::table('abouts')->insert([
//            'title' => 'contact',
//            'subtitle' => 'We are here for you',
//            'description' => 'Contact us anytime and we will be happy to answer any questions.
//
//Email: info@scoutbeyond.com',
//            'photo' => 'image_1550583111281.png',
//            'language_id' => '1',
//            'key_id' => '39',
//            'created_at' => \Carbon\Carbon::now(),
//            'updated_at' => \Carbon\Carbon::now()
//        ]);
//        DB::table('abouts')->insert([
//            'title' => 'צרו קשר',
//            'subtitle' => 'אנחנו כאן בשבילכם',
//            'description' => 'צרו איתנו קשר בכל עת ואנו נשמח לענות על כל שאלה.
//
//דוא"ל: info@scoutbeyond.com',
//            'photo' => 'image_1550583111281.png',
//            'language_id' => '2',
//            'key_id' => '39',
//            'created_at' => \Carbon\Carbon::now(),
//            'updated_at' => \Carbon\Carbon::now()
//        ]);
    }
}
