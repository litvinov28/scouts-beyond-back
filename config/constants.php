<?php

return [
    'image_folder' => [
        'avatars' => [
            'save_path' => 'public/img/avatars/',
            'get_path' => 'api/storage/img/avatars/',
        ],
        'howItWork'=> [
            'save_path' => 'public/img/howItWork/',
            'get_path' => 'api/storage/img/howItWork/',
        ],
    ],
    'video_folder' => [
        'reports' => [
            'save_path' => 'public/video/reports/',
            'get_path' => 'api/storage/video/reports/',
        ]
    ],
    'uservideo_folder' => [
        'uservideo' => [
            'save_path' => 'public/video/users/',
            'get_path' => 'api/storage/video/users/',
        ]
    ],
    'adminvideo_folder' => [
        'adminvideo' => [
            'save_path' => 'public/video/admin/',
            'get_path' => 'api/storage/video/admin/',
        ]
    ],
    'slider_folder' => [
        'sliders' => [
            'save_path' => 'public/img/sliders/',
            'get_path' => 'api/storage/img/sliders/',
        ]
    ],
    'our_team_folder' => [
        'our_teams' => [
            'save_path' => 'public/img/our_teams/',
            'get_path' => 'api/storage/img/our_teams/',
        ]
    ],
    'about_folder' => [
        'about' => [
            'save_path' => 'public/img/about/',
            'get_path' => 'api/storage/img/about/',
        ]
    ],
    'top_image_folder' => [
        'top_image' => [
            'save_path' => 'public/img/top_image/',
            'get_path' => 'api/storage/img/top_image/',
        ]
    ],
    'file_folder' => [
        'resume' => [
            'save_path' => 'public/file/resume/',
            'get_path' => 'api/storage/file/resume/',
        ]
    ],
    'file_mail_photo' => [
        'mail_photo' => [
            'save_path' => 'public/img/mail_photo/',
            'get_path' => 'api/storage/img/mail_photo',
        ]
    ],
];
