<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Mail;

//Route::get('menu','MenuController@index');
Route::get('menu','MenuController@showByKey');

Route::get('menu/{languageId}','MenuController@index');

/***CONTACT-US***/

Route::post('site/contact-us', 'ContactUsController@store');
//Route::get('site/contact-us', 'ContactUsController@index');
Route::get('site/contact-us/{language}', 'ContactUsController@index');

/***END-CONTACT-US***/

/***REGISTRATION***/

Route::post('register', 'Auth\RegisterController@register');

/***END-REGISTRATION***/

/***USER-LOGIN***/

Route::post('login', 'Auth\LoginController@authenticate');

Route::post('coach/admin/login', 'Auth\LoginController@login');

Route::post('social-login', 'Auth\LoginController@socialLogin');

/***END-USER-LOGIN***/

Route::get('user/all', 'UserController@all');

/***ADMIN-LOGIN***/

Route::post('admin/login', 'AdminController@login');
/***END-ADMIN-LOGIN***/

Route::get('user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::group(['middleware' => ['jwt.auth']], function () {
    /*** USER ***/
    Route::get('user/me', 'UserController@getMe');
    Route::get('user/delete', 'UserController@delete');
    Route::post('user/update', 'UserController@update');
    Route::post('user-update/language', 'UserController@updateLanguage');
    Route::post('user/photo', 'UserController@updatePhoto');
    Route::post('user/update/pass', 'UserController@updatePassword');
    Route::post('user/video', 'UserController@loadVideo');
    Route::post('coach/update','UserController@updateCoach');
    /*** END - USER ***/

    /*** REPORT ***/
    Route::post('report/update', 'ReportController@update');
    Route::post('report/addvideo', 'ReportController@addVideo');
    Route::post('report/create', 'ReportController@store');
    Route::get('report/myreport', 'ReportController@getReport');
    Route::post('visibility/{report}', 'ReportController@updateVisibility');
    Route::get('myreport','UserController@myreport');
    Route::get('reportPlayer','UserController@reportPlayer');
    Route::get('status','ReportController@getStatus');

    /*** END - REPORT ***/
Route::get('test/{rating}','UserController@admin');
    /*** VIDEO ***/

    Route::post('video/update', 'VideoController@update');
    Route::post('video/delete', 'VideoController@delete');
    Route::delete('uservideo/{userVideo}','UserController@deleteVideo');


    /*** END - VIDEO ***/

    Route::post('invoice','InvoiceController@store');
    /***  TEAM ***/
    Route::post('team', 'OurTeamController@store');
    /*** END - TEAM ***/


    /***      Rating   ***/
    Route::post('rating','RatingController@store');
    Route::put('rating/{rating}','RatingController@update');
    Route::delete('rating/{rating}','RatingController@delete');
    Route::get('rating/{rat}','RatingController@rating');

    /***   END   -   Rating   ***/


    /***  COMMENT  ***/
    Route::post('comment','CommentController@store');
    Route::put('comment/{comment}','CommentController@update');
    Route::delete('comment/{comment}','CommentController@delete');

    /*** END - COMMENT***/



    /*** ABOUT_US ***/
    Route::post('about','AboutController@store');
    Route::put('about/{about}','AboutController@update');
    Route::delete('about/{about}','AboutController@delete');
    /*** END - ABOUT_US ***/


    Route::group(['middleware' => 'roles', 'roles' => ['Admin']], function () {
        Route::post('import','CityController@import');
        /**  PHOTO **/
        Route::get('photo', 'PhotoController@index');
        Route::post('photo-create', 'PhotoController@store');
        Route::put('photo/{photo}', 'PhotoController@update');
        Route::delete('photo/{photo}', 'PhotoController@destroy');



        Route::get('report-video-key', 'AdminController@showByKey');


        /**  MAILS **/

        Route::post('mail', 'MailController@store');
        Route::put('mail/{mail}', 'MailController@update');
        Route::get('mails/{language}', 'MailController@index');
        Route::get('mail/{mail}', 'MailController@show');
        Route::get('mails', 'MailController@showByKey');
        Route::delete('mail/{mail}', 'MailController@destroy');

        /***LANGUAGE-SITE***/
        Route::post('language_site', 'LanguageSiteController@store');
        Route::delete('language_site/{languageSite}', 'LanguageSiteController@destroy');

        /***DYNAMIC-TEXTS***/
        Route::get('dynamic-texts', 'DynamicTextController@indexShow');

        /***PROFILE-BUTTON***/
//        Route::get('profile-buttons{language}', 'ProfileButtonController@language');
        Route::get('profile-button', 'ProfileButtonController@showByKey');
        Route::post('profile-button/create', 'ProfileButtonController@store');
        Route::post('profile-button/{button}', 'ProfileButtonController@update');
        Route::delete('profile-button/{profileButton}','ProfileButtonController@destroy');


        /***KEY***/
        Route::get('keys','ProfileButtonController@showByKey');
        Route::post('keys','KeyController@store');
        Route::get('filter/{key}', 'KeyController@filter');
        Route::get('keys', 'KeyController@index');
        Route::get('scouts-list', 'UserController@scoutList');


        /***HOW-IT-WORK***/
        Route::get('how-it-works','HowItWorkController@showByKey');
        Route::post('how-it-work','HowItWorkController@store');
        Route::put('how-it-work/{howItWork}','HowItWorkController@update');
        Route::delete('how-it-work/{howItWork}','HowItWorkController@delete');

        /***ABOUT-US***/
        Route::get('about-key','AboutController@showByKey');
        Route::get('abouts','AboutController@indexAll');





        Route::post('admin/video', 'AdminController@loadVideo');

        Route::post('admin/video/update', 'AdminController@update');
        Route::delete('admin/video/delete', 'AdminController@delete');
        Route::post('admin/video/updatetext', 'AdminController@updateText');
        Route::post('adminvideo/delete','AdminController@deleteId');

        Route::post('payment','PaymentController@store');
        Route::post('payment/update','PaymentController@update');


        Route::get('subject','SubjectController@showByKey');
        Route::put('subject/{subject}','SubjectController@update');
        Route::post('subject','SubjectController@store');
        Route::delete('subject/{subject}','SubjectController@delete');

//        Route::post('menu/update','MenuController@update');
        Route::post('menu/{menu}','MenuController@update');
        Route::post('menu','MenuController@store');
        Route::delete('menu/{menu}','MenuController@delete');

        Route::post('banner','BannerController@store');
        Route::put('banner/{banner}','BannerController@update');

        /***Сoach***/
Route::post('user/coach','UserController@createCoach');
Route::get('user/coach','UserController@getCoach');

Route::get('coach/deleted','UserController@DeletedCoach');

        /***END-Coach***/
        Route::get('slider-key', 'SliderController@showByKey');
        Route::get('slider', 'SliderController@indexAll');
        Route::post('slider', 'SliderController@store');
        Route::put('slider/{slider}', 'SliderController@update');
        Route::delete('slider/{slider}', 'SliderController@delete');

        Route::delete('team/{team}', 'OurTeamController@delete');
        Route::post('team/delete', 'OurTeamController@destroy');
        Route::put('team/{team}', 'OurTeamController@update');


        Route::post('report/coach', 'ReportController@updateByAdmin');
        Route::post('coach/delete', 'UserController@deleteCoach');

        Route::post('players/delete', 'UserController@deleteUsers');
        Route::post('admin/updatePlayer', 'UserController@updatePlayer');
        Route::post('admin/updatePassword', 'UserController@password');


        Route::post('report/delete','ReportController@delete');

        Route::get('language-key','LanguageController@showByKey');
        Route::post('language','LanguageController@store');
        Route::put('language/{language}','LanguageController@update');
        Route::delete('language/{language}','LanguageController@destroy');

        Route::post('user/restore', 'UserController@restore');
        Route::get('users/deleted', 'UserController@listDelete');


        Route::get('report/deleted', 'ReportController@listDelete');
        Route::post('report/restore', 'ReportController@restore');


        Route::post('register-scout','UserController@createScout');
        Route::post('scout-delete','UserController@deleteScout');
        Route::get('scout/deleted','UserController@DeletedScout');

    });
});

    Route::get('scout-report/{player}', 'ReportController@showDoneReport');
//Route::get('how-it-work','HowItWorkController@index');

Route::get('how-it-work/{language}','HowItWorkController@index');
Route::get('reports', 'ReportController@index');
//Route::get('about','AboutController@index');

Route::get('about/{language}','AboutController@index');
//Route::get('subjects','SubjectController@index');
Route::get('subjects/{language}','SubjectController@index');
Route::get('payment','PaymentController@index');
Route::get('admin/video', 'AdminController@getVideo');
Route::post('static-text/create', 'StaticTextController@store');
Route::post('static-text/update', 'StaticTextController@update');
Route::post('static-text/delete', 'StaticTextController@delete');

Route::get('position/count', 'PositionController@getPosition');

//Route::get('static-texts', 'StaticTextController@index');
Route::get('static-texts/{language}', 'StaticTextController@index');

Route::get('report/all', 'ReportController@getAll');

Route::post('dynamic-text/create', 'DynamicTextController@store');
Route::post('dynamic-text/update', 'DynamicTextController@update');
Route::post('dynamic-text/delete', 'DynamicTextController@delete');
//Route::get('dynamic-texts', 'DynamicTextController@index');
Route::get('dynamic-texts/{language}', 'DynamicTextController@index');


Route::post('user/search', 'UserController@search');

Route::get('slider/{language}', 'SliderController@index');

Route::get('user/video', 'UserController@getVideo');

Route::get('country', 'CountryController@index');
Route::get('city', 'CityController@index');
Route::get('city/{country}','CityController@filterCountry');

Route::get('player-types','PlayerTypeController@index');
Route::get('position', 'PositionController@index');
Route::get('position-key', 'PositionController@showByKey');
Route::get('position/{language}', 'PositionController@indexLanguage');
//Route::get('language', 'LanguageController@index');
Route::get('language/{language}', 'LanguageController@index');

/***GENDER***/
//Route::get('gender', 'GenderController@index');
Route::get('gender/{language}', 'GenderController@index');
Route::get('gender','GenderController@showByKey');


Route::get('user/{user}','UserController@getId');

/*** ***/
Route::post('password/reset','UserController@reset');
/***STATICTEXT***/

Route::get('static/{title}', 'StaticTextController@getStaticText');

/***END-STATICTEXT***/

/***USERS***/

Route::get('users', 'UserController@index');




/***END-USERS***/

/***REPORT***/

Route::get('report/{report}', 'ReportController@show');

/***END-REPORT***/

/***ADDITIONAL-INFORMATION***/

Route::get('site/amount', 'SiteController@getAmountOfPCR');

/***END-ADDITIONAL-INFORMATION***/
Route::get('team', 'OurTeamController@index');

//Route::get('banners','BannerController@index');
Route::get('banners/{language}','BannerController@index');
Route::get('banners','BannerController@indexAll');

/***LANGUAGE-SITE***/
Route::get('language_site', 'LanguageSiteController@index');

/***PROFILE-BUTTON***/
Route::get('profile-button/{language}', 'ProfileButtonController@index');



Route::get('report-video/{language}', 'AdminController@indexLanguage');







