

<html xmlns:color="http://www.w3.org/1999/xhtml">
<head>
    <link href="https://fonts.googleapis.com/css?family=Assistant:300,400,700,800" rel="stylesheet">
    <meta charset="UTF-8">
</head>
<body style="background:#f2f2f2;">

<p>&nbsp;</p>

<table align="center" cellpadding="0" cellspacing="0" dir="rtl"
       style="background: url(https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/photos/mail-bg.png) left bottom no-repeat #fff;color: #545c60;font-family: 'Assistant', sans-serif;font-size: 14pt;line-height:17pt;"
       width="600">
    <tbody>
    <tr>
        <td colspan="3"><img alt="img" src="https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/photos/header-bg2.png" width="600"/></td>
    </tr>
    <tr>
        <td colspan="3">
            <h1 style="text-align: center;color: #363636;font-weight: 800;margin: 0;padding-top: 51px;line-height: 1.4;font-size: 34px;padding-right: 16%;padding-left: 16%;background: url(https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/photos/Horizontal_Line.png) no-repeat 100% 75px; ">
                <img src="https://scouts-beyond.grassbusinesslabs.tk/api/storage/img/photos/logo.png" alt="logo" style="width: 96px;padding-right: 10px;vertical-align: bottom;"><span style="background: #ffffff;">ברוכים הבאים לאתר</span></h1>
        </td>
    </tr>
    <tr>
        <td style="width:16%">&nbsp;</td>

        <td style="text-align: center; width:66%;">
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">  היי, {{$user->firstname}} <br></p>

            <p style="padding: 10px 0px; text-align: right;">      ברוך הבא לאתר Scout Beyond<br>
                ברוך הבא לפלטפורמה האיכותית שלנו!
            </p>
            <p style="text-align: right;">כדי להיכנס בפעמים הבאות לפרופיל המשתמש שלך, תצטרך להשתמש בכתובת המייל ובסיסמה אותן סיפקת לאתר בזמן ההרשמה. לכן, דאג לרשום אותן לעצמך, או לשמור אותן בדרך אחרת. </p>
            <p style="text-align: right;padding:10px 0px">
                לאחר הכניסה המזוהה לאתר, תוכל להתחיל וליהנות מהיתרונות שהאתר מעניק לך.         </p>

            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">שחקן?</p>
            <p style="text-align: right;">דאג למלא את כל הפרטים בפרופיל השחקן שלך, וכמובן - אל תשכח להגיש בקשה לקבלת דו"ח מאמן מאחד מהמאמנים      המובילים שבצוות Scout Beyond .
                <br>                           חשוב לזכור<br>
                כי פרופילים של שחקנים שכבר קיבלו דו"ח מאמן אחד לפחות, נוטים לבלוט יותר לעיניהם של מועדונים וסוכנים.
            </p>
            <p style="text-align: right;margin: 0;color: #c5bb98;font-weight:700;">קבוצה/סקאוט/סוכן?</p>
            <p style="text-align: right;">בדף חיפוש השחקנים ישנה רשימה מלאה של השחקנים שבאתר, עם ציונים ליד כל פרופיל. תוכלו לסנן את השחקנים הרלוונטיים ביותר בשבילך, ואף לאתר שחקנים לפי פרמטרים ספציפיים דרך מנוע החיפוש שבאתר.
            </p>
            <p style="text-align: right;"> <br>    צוות Scout Beyond מאחל לך בהצלחה,
                <br>                ועומד לרשותך בכל בעיה ב-info@scoutbeyond.com

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p style="text-align: center;">
                <a href="{{url('api/user/verify', $user->verifyUser->token)}}">Verify Email</a>
            </p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

        </td>
        <td style="width:16%">&nbsp;</td>
    </tr>
    <tr style="background: url(https://scoutbeyond.com/api/storage/img/photos/footer-bg.png)   no-repeat;">
        <td colspan="3" height="24" style="padding-top: 24px;">
            <p style="color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;"> scoutbeyond.com <span
                        style="font-weight: 600;">כל הזכויות שמורות ל </span></p>
            <p style="background: #363636;color:#fff;margin: 0;text-align: center;font-weight: 300; font-size: 13px;">info@scoutbeyond.com
                <span style="font-weight: 600;">פרטי הדואר האלקטרוני שלנו</span></p>

        </td>
    </tr>
    <tr align="center" style="background: #363636;width: 600px;">
        <td colspan="3" style="text-align: center; padding-bottom: 10px;">
            <a href="https://twitter.com/BeyondScout" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                         src="https://scoutbeyond.com/api/storage/img/photos/twitter.png"/></a>
            <a href="https://www.youtube.com/channel/UC43wce-OMFE1UIFI5vxd7OQ/featured" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                           src="https://scoutbeyond.com/api/storage/img/photos/youtube.png"/></a>
            <a href="https://www.facebook.com/Scout-Beyond-409797482902478/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/facebook.png"/></a>
            <a href="https://www.instagram.com/scoutbeyond/" style="padding-left: 5px;padding-right: 5px;"><img alt="outlook" border="0"
                                                                                                                src="https://scoutbeyond.com/api/storage/img/photos/instagram.png"/></a>
        </td>
    </tr>
    </tbody>
</table>
<p>&nbsp;</p>
</body>
</html>
