<?php
return [
    'errors' => [
        'cantFindAccount' => 'We cant find an account with this credentials.',
        'failedLogin' => 'Failed to login, please try again.',
        'notExistAdmin' => 'This admin does not exist.',
        'notAdmin' => 'You are not an admin.',
    ],
    'success' => [
        'registeredUser' => 'User was successfully registered.',
        'successSubscribe' => 'Device was successfully subscribed.',
        'successLogout' => 'You have successfully logged out.',
        'successChangedPswd' => 'Password was successfully changed.',
        'updatedUser' => 'Profile was successfully changed.',
        'updatedStaticText' => 'Static text was successfully updated.',
        'createdStaticText' => 'Static text was successfully created.',
        'deletedStaticText' => 'Static text was successfully deleted.',
    ],
];
